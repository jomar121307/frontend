"use strict";

import _ from "underscore";

import AppDispatcher from "../../dispatcher/AppDispatcher.js";
import ProfileConstants from "./ProfileConstants.js";

const ProfileActions = {
  initialize : function (options) {
    AppDispatcher.handleViewAction({
      type : ProfileConstants.PROFILE_INITIALIZE,
      params : {
        options
      }
    });
  },

  reset : function () {
    AppDispatcher.handleViewAction({
      type : ProfileConstants.PROFILE_RESET
    });
  },

  loadMore : _.throttle(function () {
    AppDispatcher.handleViewAction({
      type : ProfileConstants.PROFILE_LOAD_MORE
    });
  }, 500),

  resetPagination : function () {
    AppDispatcher.handleViewAction({
      type : ProfileConstants.PROFILE_RESET_PAGINATION
    });
  },

  updateProfile : function (user) {
    AppDispatcher.handleViewAction({
      type : ProfileConstants.PROFILE_UPDATE,
      params : {
        user
      }
    });
  },

  uploadProfileVideo : function (file, progressCb) {
    AppDispatcher.handleViewAction({
      type : ProfileConstants.PROFILE_UPLOAD_PROFILE_VIDEO,
      params : {
        file,
        progressCb
      }
    });
  },

  uploadCoverVideo : function (file, progressCb) {
    AppDispatcher.handleViewAction({
      type : ProfileConstants.PROFILE_UPLOAD_COVER_VIDEO,
      params : {
        file,
        progressCb
      }
    });
  },

  checkYoutubeLink : _.debounce(function (url, type) {
    AppDispatcher.handleViewAction({
      type : ProfileConstants.PROFILE_CHECK_YOUTUBE_LINK,
      params : {
        url,
        type
      }
    });
  }, 1000),

  messageUser : function (message) {
    AppDispatcher.handleViewAction({
      type : ProfileConstants.PROFILE_MESSAGE_USER,
      params : {
        message
      }
    });
  }
};

export default ProfileActions;
