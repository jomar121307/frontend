"use strict";

import React, {PropTypes, Component} from "react";
import {render} from "react-dom";
import Slider from "react-slick";

export default class Carousel extends Component {
  constructor (props) {
    super(props);

    this.state = {
      settings : {
        dots : true,
        infinite : true,
        slidesToShow : 1,
        slidesToScroll : 1
      }
    };
  }

  render () {
    const {photos, defaultPhoto, imgClass} = this.props;

    return (
      <Slider {...this.state.settings}>
        {photos.map((photo) => {
          <div><img className={imgClass} src={_.safe(photo, "secure_url", defaultPhoto)}/></div>
        })}
      </Slider>
    )
  }
}

Carousel.propTypes = {
  photos : PropTypes.array.isRequired,
  defaultPhoto : PropTypes.string.isRequired,
  imgClass : PropTypes.string.isRequired
};
