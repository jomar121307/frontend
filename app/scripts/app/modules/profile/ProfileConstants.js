"use strict";

import keyMirror from "keymirror";

export default keyMirror({
	PROFILE_CHANGE_EVENT : null,
  PROFILE_INITIALIZE : null,
  PROFILE_RESET : null,
  PROFILE_LOAD_MORE : null,
  PROFILE_RESET_PAGINATION : null,
  PROFILE_UPDATE : null,
  PROFILE_UPLOAD_PROFILE_VIDEO : null,
  PROFILE_UPLOAD_COVER_VIDEO : null,
  PROFILE_CHECK_YOUTUBE_LINK : null,
  PROFILE_MESSAGE_USER : null,
  PROFILE_GET_ORDERS : null
});
