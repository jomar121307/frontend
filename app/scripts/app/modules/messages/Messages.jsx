"use strict";

import React from "react";
import Helmet from "react-helmet";
import {Link} from "react-router";
import moment from "moment";

import GeminiScrollbar from "react-gemini-scrollbar";
import Loader from "../../views/Loader.jsx";

import Utils from "../../../utils/utils.js";

import Thread from "./components/Thread.jsx";

import MessagesActions from "./MessagesActions.js";
import MessagesStores from "./MessagesStores.js";

export default class Messages extends React.Component {
  constructor (props) {
    super(props);
    this.state = MessagesStores.getInitialState();

    this.onChange = this.onChange.bind(this);
    this.listThreads = this.listThreads.bind(this);
    this.renderMessageBody = this.renderMessageBody.bind(this);
    this.renderNoMessageBody = this.renderNoMessageBody.bind(this);
    this.handleSendMessage = this.handleSendMessage.bind(this);
    this.handleEnterMessage = this.handleEnterMessage.bind(this);
  }

  componentWillMount () {
    MessagesStores.addChangeListener(this.onChange);
    MessagesActions.initialize(this.props.params.id);
  }

  componentWillUpdate (nextProps, nextState, nextContext) {
    if (nextContext.notifications.messages !== this.context.notifications.messages) {
      MessagesActions.updateThreads();
    }
  }

  shouldComponentUpdate (nextProps, nextState, nextContext) {
    const props = Utils.deepCompare(this.props, nextProps);
    const state = Utils.deepCompare(this.state, nextState);
    const context = Utils.deepCompare(this.context, nextContext);

    return !(props && state && context);
  }

  componentDidUpdate (prevProps, prevState) {
    const wasLoadingMore = prevState.isLoading && !this.state.isLoading;

    if (!Utils.deepCompare(this.state.selectedThread, prevState.selectedThread) && !wasLoadingMore) {
      const threadElem = document.getElementById('mainContainer').getElementsByClassName('gm-scroll-view')[0];
      threadElem.scrollTop = threadElem.scrollHeight;
    }
  }

  componentWillUnmount () {
    MessagesStores.removeChangeListener(this.onChange);
  }

  render () {
    const {threads, initialized} = this.state;

    if(!initialized){
      return <Loader colStyle={{marginTop: "50px"}} />
    }

    return (
      <div className="container m-t-s">
        <Helmet
            title={config.appname + " - Messages"}
            meta={[
              { property : "og:title", content : config.appname + " - Messages"},
              { property : "og:site_name", content : config.appname },
              ]} />
        <div className="brand-container" id="message-body">
          {!threads.length && initialized ? this.renderNoMessageBody() : this.renderMessageBody() }
        </div>
      </div>
    );
  }

  renderNoMessageBody() {
    return (
      <div className="no-message-container">
        <h3 className="text-center">
          <i className="fa fa-envelope fa-5x" aria-hidden="true"></i>
        </h3>
        <h4>You have no messages yet.</h4>
      </div>
    )
  }

  renderMessageBody() {
    const {selectedThread, threads, isLoading} = this.state;
    const otherUser = !_.safe(selectedThread, "users.0.__self") &&
      _.safe(selectedThread, "users.0") || _.safe(selectedThread, "users.1") || {};
    return (
      <div className="div-container">
        <div className="row">
          <div className="col-sm-3 col-xs-5 thread-list-container">
            <GeminiScrollbar className="thread-list" autoshow={false}>
              <div className="media">
                {threads.map(this.listThreads)}
              </div>
            </GeminiScrollbar>
          </div>
          <div className="col-sm-9 col-xs-7 main-conversation-container">
            <div className="" id="mainContainer">
              <div className="thread-header">
                <Link to={"shop/" + otherUser.id} className="text-decor-none">
                  <h3>{_.safe(otherUser, "firstName", "")} {_.safe(otherUser, "lastName", "")}</h3>
                </Link>
                <hr/>
              </div>
              <GeminiScrollbar className="thread-container" autoshow={false}>
                <Thread thread={selectedThread}
                  isLoading={this.isLoading}
                  loadMore={MessagesActions.loadMore} />
              </GeminiScrollbar>
              <div className="row">
                <div className="col-sm-12 message-box">
                  <textarea placeholder="You message here (You may press Enter to send.)"
                    ref="message"
                    className="form-control send-message-textbox"
                    onKeyPress={this.handleSendMessage}>
                  </textarea>
                  <button className="btn btn-primary pull-right btn-sendmessage" onClick={this.handleEnterMessage}>
                    send message
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  onChange () {
    this.setState(MessagesStores.getState());
  }

  listThreads (thread) {
    const otherUser = !thread.users[0].__self && thread.users[0] || thread.users[1];
    let thumb = _.safe(otherUser, "metadata.profileVideo.secure_url", config.defaultYoutubeProfileVideo);
    let lastestMsg = thread.messages[thread.messages.length - 1];
    let timeFormat = "h:mma";

    if (!lastestMsg || new Date() < new Date(lastestMsg.createdAt)) {
      lastestMsg = {createdAt : new Date()};
      timeFormat = "ddd";
    } else if (moment().date() - moment(lastestMsg.createdAt).date() > 0 &&
      moment().diff(moment(lastestMsg.createdAt), "days") < 7) {
      timeFormat = "ddd";
    } else if (moment().diff(moment(lastestMsg.createdAt), "days") > 7 &&
      moment().year() - moment(lastestMsg.createdAt).year() < 1) {
      timeFormat = "MMM D";
    } else if (moment().year() - moment(lastestMsg.createdAt).year() >= 1) {
      timeFormat = "MM/DD/YY";
    }

    if (_.safe(otherUser, "metadata.profileVideo.resource_type", "youtube") === "youtube") {
      thumb = cloudinary.url(`https://i.ytimg.com/vi/${thumb}/default.jpg`, {
        height: 45,
        width: 45,
        crop: "fill",
        type : "fetch"
      });
    } else if (_.safe(otherUser, "metadata.profileVideo.resource_type") === "video") {
      const profileVideo = otherUser.metadata.profileVideo;

      thumb = cloudinary.url(`${profileVideo.public_id}.jpg`, {
        height: 45,
        transformation: ["media_lib_thumb"],
        width: 45,
        crop: "fill",
        resource_type: "video",
        secure : true
      });
    }

    return (
      <div className="row" key={thread.id + _.random(9999999999)} onClick={this.handleSelectThread.bind(null, thread.id)}>
        <div className="col-sm-12 p-v-s">
          <div className="media-left">
            <a href="#">
              <img className="media-object" src={thumb} alt={`${otherUser.firstName} ${otherUser.lastName || ""}`} />
            </a>
          </div>
          <div className="media-body">
            <h4 className="media-heading">
              <span className="chat-name">{otherUser.firstName} {otherUser.lastName || ""} </span>
              <span className="sent-message">
                • {moment(lastestMsg.createdAt).format(timeFormat)}
                <span className="text-danger hide"> ({thread.unreadCount})</span>
              </span>
            </h4>
            <p className="ellipsis">{_.prune(_.safe(lastestMsg, "payload.message", ""), 35)}</p>
          </div>
        </div>
      </div>
    )
  }

  handleSelectThread (threadId, e) {
    MessagesActions.selectThread(threadId);
  }

  handleSendMessage (e) {
    const {selectedThread} = this.state;
    if (_.isEmpty(selectedThread)) {
      e.preventDefault();
      return;
    } else if (e.which === 13 && !e.shiftKey) {
      e.preventDefault();
      MessagesActions.send(e.target.value.trim());
      this.refs["message"].value = "";
    }
  }

  handleEnterMessage (e) {
    const {selectedThread} = this.state;

    if (_.isEmpty(selectedThread)) {
      e.preventDefault();
      return;
    }

    MessagesActions.send(this.refs["message"].value);
    this.refs["message"].value = "";
  }
}

module.exports = Messages;

Messages.contextTypes = {
  notifications : React.PropTypes.object.isRequired
}
