"use strict";

import async from "async";
import {EventEmitter} from "events";
import superagent from "superagent";

import Request from "../../../../../utils/request.js";
import {register} from "../../../../dispatcher/AppDispatcher.js";

import ShopSettingsConstants from "./ShopSettingsConstants.js";
import AppActions from "../../../../actions/AppActions.js";

const initialState = {
  initialized : false,
  paypalInfo : {},
  eCashInfo : {},
  info : {},
  shopName : "",
  error : {},
  hasRightToSell : false
};

let state = $.extend(true, {}, initialState);

let Stores = Object.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(ShopSettingsConstants.SHOPSETTINGS_CHANGE_EVENT, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(ShopSettingsConstants.SHOPSETTINGS_CHANGE_EVENT, callback);
  },

  _emitChange : function () {
    Stores.emit(ShopSettingsConstants.SHOPSETTINGS_CHANGE_EVENT);
  },

  getInitialState : function () {
    return initialState;
  },

  getState : function () {
    return state;
  }
});

const Actions = {
  [ShopSettingsConstants.SHOPSETTINGS_INITIALIZE] : function (params) {
    shopSettingsInitialize(function (err, data) {
      if (err) {
        return shopSettingsSetState(err, {});
      }

      shopSettingsSetState(null, {
        initialized : true,
        paypalInfo : data.paypalInfo || {},
        eCashInfo : data.eCashInfo || {},
        hasRightToSell : !_.isEmpty(data.paypalInfo) || !_.isEmpty(data.eCashInfo),
        shopName : data.shopName
      });

      AppActions.setPaypalInfoFlag(!_.isEmpty(state.paypalInfo) || !_.isEmpty(state.eCashInfo))
    });
  },

  [ShopSettingsConstants.SHOPSETTINGS_SET_INFO] : function (params) {
    shopSettingsSetState(null, {
      paypalInfo : params.paypalInfo || state.paypalInfo,
      eCashInfo : params.eCashInfo || state.eCashInfo,
      error : params.error || {}
    });
  },

  [ShopSettingsConstants.SHOPSETTINGS_SAVE] : function (params) {
    shopSettingsSave(params, function (err, data) {
      if (err) {
        AppActions.setPaypalInfoFlag(!_.isEmpty(state.paypalInfo) || !_.isEmpty(state.eCashInfo))
        return shopSettingsSetState(err, {
          paypalInfo : state.paypalInfo,
          eCashInfo : state.eCashInfo
        });
      }

      shopSettingsSetState(null, {
        hasRightToSell : data.status === "success",
      })

      AppActions.setPaypalInfoFlag(true);
      toastr.success("Successfully saved settings.");
    });
  }
}

Stores.dispatchIndex = register(payload => {
  const {type, params} =  payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function shopSettingsSetState (err, obj) {
  if (err) {
    toastr.error(_.safe(err, "xhr.responseText", err));
    console.error(err);
  }

  for (var prop in obj) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = obj[prop];
    } else {
      console.warn("Invalid state for shopsettings state: " + prop);
    }
  }

  Stores._emitChange(ShopSettingsConstants.SHOPSETTINGS_CHANGE_EVENT);
}

function shopSettingsInitialize (callback) {
  Request.get("shopsettings", callback);
}

function shopSettingsSave (options, callback) {
  Request.put("shopsettings", options.info, callback);
}
