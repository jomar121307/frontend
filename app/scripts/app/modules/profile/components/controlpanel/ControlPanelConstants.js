"use strict";

import keyMirror from "keymirror";

export default keyMirror({
  CONTROLPANEL_CHANGE_EVENT : null,
  CONTROLPANEL_SET_CATEGORIES : null,
  CONTROLPANEL_SELECT_CATEGORY : null,
  CONTROLPANEL_SHOW_MODAL : null,
  CONTROLPANEL_SAVE_CATEGORY : null,
  CONTROLPANEL_DELETE_CATEGORY : null
});
