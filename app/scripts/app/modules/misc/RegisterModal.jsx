"use strict"

import React from "react";

export default class RegisterModal extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      error : {},
      registering : false
    }

    this.handleRegister = this.handleRegister.bind(this);
  }

  componentWillReceiveProps (nextProps) {
    let {error} = this.state;

    error = _.merge(error, nextProps.error);

    this.setState({
      error
    });

    if (!this.props.complete && nextProps.complete) {
      _.each(this.refs, (v, k) => {
        v.value = "";
      });
      $("#registerModal").modal("hide");
    }
  }

  render () {
    const {error, registering} = this.state;

    return (
      <div className="modal fade" id="registerModal" tabIndex="-1" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 className="modal-title">
                Register for <img className="" src="images/logo3.png" />
              </h4>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className={"form-group col-sm-6 " + (error.email ? "has-error" : "")}>
                  <div className="row">
                    <div className="col-sm-12">
                      <p className="text-left bold cpp-label lh-standard no-margin">
                        Email<span>*</span>
                      </p>
                    </div>
                    <div className="col-sm-12">
                      <div className="m-b-s text-center">
                        <input type="text"
                          className="form-control cpp-pet-info-input"
                          ref="email"
                          disabled={registering ? "disabled" : ""}/>
                        <label className="control-label pull-left">{error.email}</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={"form-group col-sm-6 " + (error.username ? "has-error" : "")}>
                  <div className="row">
                    <div className="col-sm-12">
                      <p className="text-left bold cpp-label lh-standard no-margin">
                        Username<span>*</span>
                      </p>
                    </div>
                    <div className="col-sm-12">
                      <div className="m-b-s text-center">
                        <input type="text"
                          className="form-control cpp-pet-info-input"
                          ref="username"
                          disabled={registering ? "disabled" : ""}/>
                        <label className="control-label pull-left">{error.username}</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className={"form-group col-sm-6 " + (error.password ? "has-error" : "")}>
                  <div className="row">
                    <div className="col-sm-12">
                      <p className="text-left bold cpp-label lh-standard no-margin">
                        Password<span>*</span>
                      </p>
                    </div>
                    <div className="col-sm-12">
                      <div className="m-b-s text-center">
                        <input type="password"
                          className="form-control cpp-pet-info-input"
                          ref="password"
                          disabled={registering ? "disabled" : ""}/>
                        <label className="control-label pull-left">{error.password}</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={"form-group col-sm-6 " + (error.confirmPassword ? "has-error" : "")}>
                  <div className="row">
                    <div className="col-sm-12">
                      <p className="text-left bold cpp-label lh-standard no-margin">
                        Confirm Password<span>*</span>
                      </p>
                    </div>
                    <div className="col-sm-12">
                      <div className="m-b-s text-center">
                        <input type="password"
                          className="form-control cpp-pet-info-input"
                          ref="confirmPassword"
                          disabled={registering ? "disabled" : ""}/>
                        <label className="control-label pull-left">{error.confirmPassword}</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className={"form-group col-sm-6 " + (error.firstName ? "has-error" : "")}>
                  <div className="row">
                    <div className="col-sm-12">
                      <p className="text-left bold cpp-label lh-standard no-margin">
                        First Name<span>*</span>
                      </p>
                    </div>
                    <div className="col-sm-12">
                      <div className="m-b-s text-center">
                        <input type="text"
                          className="form-control cpp-pet-info-input"
                          ref="firstName"
                          disabled={registering ? "disabled" : ""}/>
                        <label className="control-label pull-left">{error.firstName}</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={"form-group col-sm-6 " + (error.middleName ? "has-error" : "")}>
                  <div className="row">
                    <div className="col-sm-12">
                      <p className="text-left bold cpp-label lh-standard no-margin">
                        Middle Name:
                      </p>
                    </div>
                    <div className="col-sm-12">
                      <div className="m-b-s text-center">
                        <input type="text"
                          className="form-control cpp-pet-info-input"
                          ref="middleName"
                          disabled={registering ? "disabled" : ""}/>
                        <label className="control-label pull-left">{error.middleName}</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className={"form-group col-sm-6 " + (error.lastName ? "has-error" : "")}>
                  <div className="row">
                    <div className="col-sm-12">
                      <p className="text-left bold cpp-label lh-standard no-margin">
                        Last Name<span>*</span>
                      </p>
                    </div>
                    <div className="col-sm-12">
                      <div className="m-b-s text-center">
                        <input type="text"
                          className="form-control cpp-pet-info-input"
                          ref="lastName"
                          disabled={this.state.registering ? "disabled" : ""}/>
                        <label className="control-label pull-left">{error.lastName}</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={"form-group col-sm-6 " + (error.gender ? "has-error" : "")}>
                  <div className="row">
                    <div className="col-sm-12">
                      <p className="text-left bold cpp-label lh-standard no-margin">
                        Gender
                      </p>
                    </div>
                    <div className="col-sm-12">
                      <div className="m-b-s text-center">
                        <select className="form-control cpp-pet-info-input"
                          ref="gender"
                          disabled={registering ? "disabled" : ""}>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                        <label className="control-label pull-left">
                          {error.gender}
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <div className="row">
                <div className="col-md-12 no-pad">
                  <button type="button"
                    className="btn btn-default pull-left bold"
                    data-dismiss="modal">
                      Cancel
                  </button>
                  <button type="button"
                    className="btn btn-primary bold"
                    onClick={this.handleRegister}>Register</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  handleRegister (e) {
    e.preventDefault();
    let user = {};
    let error = {};
    const {handleRegister} = this.props;

    if (!this.refs["email"].value.trim()) {
      error.email = "Email address is required.";
    }

    if (!this.refs["username"].value.trim()) {
      error.username = "Username is required.";
    }

    if (!this.refs["password"].value.trim()) {
      error.password = "Password is blank.";
      error.confirmPassword = "Password is blank.";
    } else if (this.refs["password"].value.trim().length < 8) {
      error.password = "Password must be at least 8 characters.";
      error.confirmPassword = "Password must be at least 8 characters.";
    }

    if (this.refs["password"].value.trim() !== this.refs["confirmPassword"].value.trim()) {
      error.confirmPassword = "Password is not the same.";
    }

    if (!this.refs["firstName"].value.trim()) {
      error.firstName = "First name is required.";
    }

    if (!this.refs["lastName"].value.trim()) {
      error.lastName = "Last name is required.";
    }

    if (!_.isEmpty(error)) {
      this.setState({
        error : error
      });

      return;
    }

    _.each(this.refs, (v, k) => {
      user[k] = v.value.trim();
    });

    this.setState({
      error : {}
    });

    handleRegister(user);
  }
}
