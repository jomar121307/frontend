
//
// Dependencies
//
import React from 'react';
import ClassNames from "classnames";
//
// Components
//

//
// Flux Components
//
import ResetPasswordActions from "./ResetPasswordActions.js";
import ResetPasswordStores from "./ResetPasswordStores.js";

export default class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = ResetPasswordStores.getInitialState();
    this.onChange = this.onChange.bind(this);

    this.renderView = this.renderView.bind(this);
    this.renderUnsent = this.renderUnsent.bind(this);
    this.renderSent = this.renderSent.bind(this);

    this.handleFormChange = this.handleFormChange.bind(this);
    this.handleSendEmail = this.handleSendEmail.bind(this);
    this.handleSetPassword = this.handleSetPassword.bind(this);
  }

  componentWillMount () {
    const {location} = this.props;

    ResetPasswordStores.addChangeListener(this.onChange);

    if (location.query.rt) {
      ResetPasswordActions.showResetFields();
    }
  }

  componentWillReceiveProps(nextProps, nextState) {
    const location = this.props.location;
    const nextLocation = nextProps.location;

    if (location.key !== nextLocation.key) {
      ResetPasswordActions.reset();
    }
  }

  componentWillUnmount () {
    ResetPasswordActions.reset();
    ResetPasswordStores.removeChangeListener(this.onChange);
  }

  onChange () {
    this.setState(ResetPasswordStores.getState())
  }

  render() {

    return (
      <div className="module-container" id="resetpassword-container">
        {this.renderView()}
      </div>
    )
  }

  renderView () {
    const {renderView} = this.state;

    switch(renderView) {
      case "unsent":
        return this.renderUnsent();
      case "sent":
        return this.renderSent();
      case "resetfields":
        return this.renderResetFields();
      case "resetsuccess":
        return this.renderResetSuccess()
      default:
        return ""
    }
  }

  renderUnsent () {
    const {email, error, isRequesting} = this.state;

    return (
      <div className="container">
        <h3>Forgot password</h3>
        <hr className="no-m-t rp-hr"/>
        <div className="row">
          <div className="col-sm-4">
            <div className={ClassNames("form-group", {"has-error" : error.email})}>
              <label htmlFor="email">Email address</label>
              <input type="text"
                className="form-control"
                ref="email"
                value={email}
                onChange={this.handleFormChange.bind(null, "email")}
                placeholder="Email"/>
              <label className={ClassNames("control-label", {hide : !error.email})}
                htmlFor="FirstName">
                  {error.email}
              </label>
            </div>
            <p className="submit-email">Confirmation email will be sent shortly after submission.</p>
            <button className={ClassNames("btn btn-primary", {disabled : isRequesting})}
              onClick={this.handleSendEmail}>
                {isRequesting ? "Submitting.." : "Submit"}
            </button>
          </div>
        </div>
      </div>
    )
  }

  renderSent () {
    return (
      <div className="container">
        <h3>Reset password email sent.</h3>
        <hr className="no-m-t rp-hr"/>
        <div className="row">
          <div className="col-sm-4">
            <p className="submit-email">We have sent you an email with the link.</p>
          </div>
        </div>
      </div>
    )
  }

  renderResetFields () {
    const {error, password, confirmPassword, isResetingPassword} = this.state;

    return (
      <div className="container">
        <h3>Reset your password</h3>
        <hr className="no-m-t rp-hr"/>
        <div className="row">
          <div className="col-sm-4">
            <div className={ClassNames("form-group", {"has-error" : error.password})}>
              <label htmlFor="password">New password</label>
              <input type="password"
                className="form-control"
                value={password}
                onChange={this.handleFormChange.bind(null, "password")}
                placeholder="New password"/>
              <label className={ClassNames("control-label", {hide : !error.password})}
                htmlFor="password">
                  {error.password}
              </label>
            </div>
            <div className={ClassNames("form-group", {"has-error" : error.confirmPassword})}>
              <label htmlFor="confirmPassword">Confirm password</label>
              <input type="password"
                className="form-control"
                value={confirmPassword}
                onChange={this.handleFormChange.bind(null, "confirmPassword")}
                placeholder="Confirm password"/>
              <label className={ClassNames("control-label", {hide : !error.confirmPassword})}
                htmlFor="confirmPassword">
                  {error.confirmPassword}
              </label>
            </div>
            <p className="submit-email">Your new password must have 8 or more characters.</p>
            <button className={ClassNames("btn btn-primary", {disabled : isResetingPassword})}
              onClick={this.handleSetPassword}>
                {isResetingPassword ? "Reseting.." : "Reset"}
            </button>
          </div>
        </div>
      </div>
    )
  }

  renderResetSuccess () {
    return (
      <div className="container">
        <h3>Your password has been changed.</h3>
        <hr className="no-m-t rp-hr"/>
        <div className="row">
          <div className="col-sm-4">
            <p className="submit-email">An email will be sent as confirmation.</p>
          </div>
        </div>
      </div>
    )
  }

  handleFormChange (type, e) {
    const value = e.target.value.trim();
    let error = {};
    let obj = {
      password : this.state.password,
      confirmPassword : this.state.confirmPassword,
      email : this.state.email
    };

    switch (type) {
      case "email":
        if (!value) {
          error.email = "Email address is required.";
        }

        obj.email = value;
        break;
      case "password":
        if (!value) {
          error.password = "Password is required.";
        }

        obj.password = value;
        break;
      case "confirmPassword":
        if (!value) {
          error.confirmPassword = "Password is required.";
        }

        obj.confirmPassword = value;
        break;
    }

    if (!_.isEmpty(error)) {
      return ResetPasswordActions.setForm({
        error,
        form : obj
      });
    }

    ResetPasswordActions.setForm({
      form : obj,
      error : {}
    });
  }

  handleSendEmail (e) {
    e.preventDefault();
    const {email, isRequesting, error} = this.state;

    if (!_.isEmpty(error) || isRequesting) {
      return;
    } else if (!config.emailRegex.test(email)) {
      return ResetPasswordActions.setForm({
        error : {
          email : "Email address is invalid."
        }
      });
    }

    ResetPasswordActions.resetPassword();
  }

  handleSetPassword () {
    const {password, confirmPassword} = this.state;
    let {error} = this.state;

    if (!_.isEmpty(error)) {
      return;
    }

    if (password.length < 8) {
      error.password = "Password must have at least 8 characters.";
    }

    if (confirmPassword !== password) {
      error.password = "Passwords don't match.";
      error.confirmPassword = "Passwords don't match.";
    }

    if (!_.isEmpty(error)) {
      return ResetPasswordActions.setForm({
        error,
        form : {
          password : password,
          confirmPassword : confirmPassword
        }
      });
    }

    ResetPasswordActions.setPassword({
      rt : this.props.location.query.rt
    });
  }
}
