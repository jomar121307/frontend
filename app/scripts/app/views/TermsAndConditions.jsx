"use strict";

//
// Dependencies
//
var React = require("react");

var TermsAndCondition = React.createClass({
	render: function (){
		return(
			<div className="brand-container hideSidebar" >
		      <div className="outer-container">
		        <div className="row">
		          <div className="col-sm-12">
		            <div className="panel panel-default">
					  	<div className="panel-heading"><h3>{config.appname} Terms and Condition</h3></div>
					    <div className="panel-body">
						<p>Last updated: November 10, 2015</p>
						<p>
							Please read these Terms and Conditions carefully before using
							the https://www.bentanayan.com website and its mobile application operated by Bentanayan.
							Your access to and use of the Service is conditioned on your acceptance of and compliance with
							these Terms. These Terms apply to all visitors, users and others who access or use the Service.
							<h4>By accessing or using the Service you agree to be bound by these Terms. If you disagree
							with any part of the terms then you may not access the Service.</h4>
							<h4>Basic Terms</h4>
							<ul>
								<li>You are responsible for any activity that occurs under your screen name.</li>
								<li>You are responsible for keeping your password secure.</li>
								<li>You are allowed to have only one personal user account.</li>
								<li>You must not abuse, harass, threaten, impersonate or intimidate other {config.appname}.com users.</li>
								<li>You may not use the {config.appname}.com service for any illegal or unauthorized purpose. International users agree to comply with all local laws regarding online conduct and acceptable content.</li>
								<li>You are solely responsible for your conduct and any data, text, information, screen names, graphics, photos, profiles, audio and video clips, links ('Content') that you submit, post, and display on the feeds of {config.appname}.com.</li>
								<li>You must not modify, adapt or hack {config.appname}.com or modify another website so as to falsely imply that it is associated with {config.appname}.com.</li>
								<li>You must not create or submit unwanted email or advertisements to {config.appname} or any of its members ('Spam').</li>
								<li>You must not transmit any worms or viruses or any code of a destructive nature.</li>
								<li>You must not, in the use of {config.appname}, violate any laws in your jurisdiction (including but not limited to copyright laws).</li>
								<li>{"You'll"} not copy our website theme or layout.</li>
								<li>{"You'll"} not post discussion topics language other than English.</li>
								<li>{"You'll"} not use invalid names.</li>
								<li>Violation of any of these agreements will result in the termination of your {config.appname}.com account.</li>
							</ul>
							<h4>Purchases</h4>
							<div className="pdleft">
								If you wish to purchase any product or service made available through the Service ("Purchase"),
								you may be asked to supply certain information relevant to your Purchase including, without
								limitation.
							</div>
							<h4>Subscriptions</h4>
							<div className="pdleft">
								Some parts of the Service are billed on a subscription basis ("Subscription(s)"). You will be billed in
								advance on a recurring...
							</div>
							<h4>Content</h4>
							<div className="pdleft">
								Our Service allows you to post, link, store, share and otherwise make available certain information,
								text, graphics, videos, or other material ("Content"). You are responsible for.
							</div>
							<h4>Links To Other Web Sites</h4>
							<div className="pdleft">
								Our Service may contain links to third­party web sites or services that are not owned or controlled
								by {config.appname} Inc.
								{config.appname} Inc. has no control over, and assumes no responsibility for, the content,
								privacy policies, or practices of any third party web sites or services. You further acknowledge and
								agree that {config.appname} Inc. shall not be responsible or liable, directly or indirectly, for any
								damage or loss caused or alleged to be caused by or in connection with use of or reliance on any
								such content, goods or services available on or through any such web sites or services.
							</div>
							<h4>Changes</h4>
							<div className="pdleft">
								We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a
								revision is material we will try to provide at least 30 days  notice prior to any new terms
								taking effect. What constitutes a material change will be determined at our sole discretion.
							</div>
							<h4>Contact Us</h4>
							<div className="pdleft">
								If you have any questions about these Terms, please contact us.
							</div>
						</p>
					  </div>
				    </div>
				  </div>
			    </div>
			  </div>
			</div>
		);
	}
});

module.exports = TermsAndCondition;
