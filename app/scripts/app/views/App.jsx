"use strict";

import React, {Component} from 'react';
import {render} from 'react-dom';
import {Navigation, Link} from "react-router";

// Components
import Header from "./Header.jsx";
import Footer from "./Footer.jsx";

import AppActions from "../actions/AppActions.js";
import AppStores from "../stores/AppStores.js";

export default class App extends Component {
  constructor () {
    super();
    this.state = AppStores.getInitialState();
    this.onChange = this.onChange.bind(this);
  }

  getChildContext () {
    return {
      notifications : this.state.notifications,
      isLoggedIn : this.state.isLoggedIn,
      categories : this.state.categories,
    };
  }

  componentWillMount () {
    AppStores.addChangeListener(this.onChange);
    AppActions.initialize();
    AppActions.getNotifications();

    this.initializeInterval = setInterval(function () {
      if (!config.isLocal) {
        AppActions.getNotifications();
      }
    }, 1000);
  }

  componentWillUnmount () {
    clearInterval(this.initializeInterval);
    AppStores.removeChangeListener(this.onChange);
  }

  render () {
    return (
      <div>
        <div className="app-container">
          <Header {...this.state} location={this.props.location}/>
          {this.props.children}
        </div>
        <Footer />
      </div>
    )
  }

  onChange () {
    this.setState(AppStores.getState());
  }
}

App.childContextTypes = {
  notifications : React.PropTypes.object,
  isLoggedIn : React.PropTypes.bool,
  categories : React.PropTypes.array,
};
