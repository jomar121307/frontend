"use strict";

import React, {Component, PropTypes} from "react";

export default class Input extends Component {
  constructor (props) {
    super(props);
    this.state = {
      value : props.value
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      value : nextProps.value
    });
  }

  shouldComponentUpdate (nextProps, nextState) {
    if (this.state.value === nextProps.value) {
      return false;
    }

    return true;
  }

  render () {
    return <input {...this.props}/>
  }
}
