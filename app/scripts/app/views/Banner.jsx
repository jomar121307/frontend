"use strict";

var React = require("react");
var OverlayTrigger = require("react-bootstrap").OverlayTrigger;
var Router = require("react-router");
var Navigation = Router.Navigation;

var Banner = React.createClass({

	getInitialState : function () {
		return {
			isMobile : false
		}
	},

	componentDidMount : function() {
    var mql = window.matchMedia("screen and (max-width: 495px)");

    var that = this;
    viewportChecker(mql); //initialize

    function viewportChecker (mql) {
      if(mql.matches){
        $('#promote-banner').show();
        that.setState({
	        isMobile : true
        })
      }
    }
  },

	render : function () {
		if (!this.state.isMobile) {
			return <div/>;
		}
		return (
			<div id="promoteBanner" className="">
				<div className="row">
					<div className="col-xs-1 col-sm-1">
						<button type="button" className="close-banner form-control" onClick={this._closePromote} aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
          <div className="col-xs-3 col-sm-1">
            <img src="images/logoTrans_blueBG_wBorders-01.png" className="promote-img"/>
          </div>
          <div className="col-xs-6 col-sm-2">
          	<span className="promote-info">
	            <h4 className="no-margin"><b>Petgago</b></h4>
	            <p className="no-margin">Get on the App Store</p>
	            <p>NOT INSTALLED</p>
          	</span>
          </div>
          <div className="col-xs-2 col-offset-sm-8 col-sm-1">
          	<span className="pull-right text-left promote-view-btn">
	          	<a href="#">
	          		<span>View</span>
	          	</a>
            </span>
          </div>
        </div>
			</div>
		)
	},

	_closePromote : function () {
		$("#promoteBanner").addClass("hide");
	}

});

module.exports = Banner;
