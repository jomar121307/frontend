"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import ReactTooltip from "react-tooltip";

import Utils from "../../../../utils/utils.js";

import TagsInput from "./TagsInput.jsx";

export default class ProductInformation extends Component {
  constructor (props) {
    super (props);
  }

  shouldComponentUpdate (nextProps, nextState) {
    return !Utils.deepCompare(nextProps, this.props);
  }

  render () {
    const {
      errors,
      quantity,
      status,
      tags,
      price,
      resellCommission,
      handleInputChange,
      handleTagsChange,
      handleCommissionTypeChange
    } = this.props;

    return (
      <div className="brand-container">
        <div className="outer-container">
          <div className="div-container">
            <div className="row">
              <div className="col-sm-12">
                <h4>Product Information</h4>
                <hr/>
                <div className="row">
                  <form className="form-horizontal">
                    <div className="col-sm-6">
                      {this.renderCategories()}
                      {this.renderSubcategories()}
                      {this.renderSubSquared()}
                      <div className={classNames("form-group", {"has-error" : errors.quantity})}>
                        <label htmlFor="quantity" className="col-sm-3 control-label">
                          Stock Quantity :
                        </label>
                        <div className="col-sm-6">
                          <input className={classNames("form-control",
                            {"input-has-error" : errors.quantity})}
                            onChange={handleInputChange.bind(null, "quantity")}
                            value={quantity}
                            data-tip
                            data-for={!!!errors.quantity || "productForm_quantity"}/>
                          <ReactTooltip id="productForm_quantity" type="error" effect="solid">
                            <span>{errors.quantity}</span>
                          </ReactTooltip>
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="status" className="col-sm-3 control-label">
                          Status :
                        </label>
                        <div className="col-sm-6">
                          <select className="form-control"
                            value={status}
                            onChange={handleInputChange.bind(null, "status")}>
                              <option value="available">Available</option>
                              <option value="limited">Limited</option>
                              <option value="outofstock">Out of Stock</option>
                          </select>
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="tags" className="col-sm-3 control-label">
                          Tags :
                        </label>
                        <div className="col-sm-6">
                          <TagsInput className="form-control" tags={tags}
                            onChange={handleTagsChange}/>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className={classNames("form-group", {"has-error" : errors.price})}>
                        <label htmlFor="price" className="col-sm-3 control-label">
                          Product Price :
                        </label>
                        <div className="col-sm-6">
                          <div className="input-group">
                            <ReactTooltip id="productForm_price" type="error" effect="solid">
                              <span>{errors.price}</span>
                            </ReactTooltip>
                            <input type="number"
                              className={classNames("form-control", {"input-has-error" : errors.price})}
                              onChange={handleInputChange.bind(null, "price")}
                              value={price}
                              aria-describedby="price"
                              data-tip
                              data-for={!!!errors.price || "productForm_price"}/>
                            <span className="input-group-addon" id="price">PHP</span>
                          </div>
                        </div>
                      </div>
                      <div className={classNames("form-group", {"has-error" : errors.resell})}>
                        <label className="col-sm-3 col-xs-12 control-label">
                          Resell Commission :
                        </label>
                        <div className="col-sm-6 col-xs-6">
                          <input type="number"
                            className={classNames("form-control", {"input-has-error" : errors.resell})}
                            onChange={handleInputChange.bind(null, "resell")}
                            value={resellCommission.value}
                            data-tip
                            data-for={!!!errors.resell || "productForm_resell"}/>
                          <ReactTooltip id="productForm_resell" type="error" effect="solid">
                            <span>{errors.resell}</span>
                          </ReactTooltip>
                        </div>
                        <div className="col-sm-3 col-xs-6">
                          <select className="form-control"
                            value={resellCommission.type}
                            onChange={handleCommissionTypeChange}>
                            <option>percentage</option>
                            <option>fixed</option>
                          </select>
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="status" className="col-sm-3 control-label">
                          Processing Time :
                        </label>
                        <div className="col-sm-6">
                          <select className="form-control">
                            <option value="1BD">1 business day</option>
                            <option value="1-2BD">1-2 business days</option>
                            <option value="1-3BD">1-3 business days</option>
                            <option value="3-5BD">3-5 business days</option>
                            <option value="1-2W">1-2 weeks</option>
                            <option value="2-3W">2-3 weeks</option>
                            <option value="3-4W">3-4 weeks</option>
                            <option value="4-6W">4-6 weeks</option>
                            <option value="6-8W">6-8 weeks</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderCategories () {
    const {handleInputChange, errors, categories} = this.props;
    let {selectedCategory} = this.props;
    selectedCategory = selectedCategory.split(".");

    return (
      <div className={classNames("form-group", {"has-error" : errors.category})}>
        <label htmlFor="category" className="col-sm-3 control-label">
          Category :
        </label>
        <div className="col-sm-6">
          <select className={classNames("form-control", {"select-has-error" : errors.category})}
            data-tip
            data-for={!!!errors.category || "productForm_category"}
            value={selectedCategory[0] || ""}
            onChange={handleInputChange.bind(null, "category")}>
              <option value="" hidden>Select Category</option>
              {categories.map(cat =>
                <option value={cat.slug} key={cat.slug}>
                  {cat.name}
                </option>
              )}
          </select>
          <ReactTooltip id="productForm_category" type="error" effect="solid">
            <span>{errors.category}</span>
          </ReactTooltip>
        </div>
      </div>
    )
  }

  renderSubcategories () {
    const {handleInputChange, errors, categories} = this.props;
    let {selectedCategory} = this.props;
    selectedCategory = selectedCategory.split(".");
    const idx = _.findIndex(categories, {
      slug : selectedCategory[0]
    });
    let category = categories[idx] || {subcategories : []};

    if (idx === -1) {
      return <div/>
    }

    return (
      <div className={classNames("form-group", {"has-error" : errors.subcategory})}>
        <label htmlFor="category" className="col-sm-3 control-label">
          Subcategory :
        </label>
        <div className="col-sm-6">
          <select className={classNames("form-control", {"select-has-error" : errors.subcategory})}
            data-tip
            data-for={!!!errors.subcategory || "productForm_subcategory"}
            value={selectedCategory[1] || ""}
            onChange={handleInputChange.bind(null, "subcategory")}>
              <option value="" hidden>Select Category</option>
              {category.subcategories.map(subcat =>
                <option key={subcat.groupheader.slug}
                  value={subcat.groupheader.slug}>
                    {subcat.groupheader.name}
                </option>
              )}
          </select>
          <ReactTooltip id="productForm_subcategory" type="error" effect="solid">
            <span>{errors.subcategory}</span>
          </ReactTooltip>
        </div>
      </div>
    )
  }

  renderSubSquared () {
    const {handleInputChange, errors, categories} = this.props;
    let {selectedCategory} = this.props;
    selectedCategory = selectedCategory.split(".");
    const idx = _.findIndex(categories, {
      slug : selectedCategory[0]
    });
    const category = categories[idx] || {subcategories : []};
    const subcategory = _.filter(category.subcategories, subcat => subcat.groupheader.slug === selectedCategory[1]);

    if (subcategory.length !== 1 || !subcategory[0].groupling.length) {
      return <div/>
    }

    return (
      <div className={classNames("form-group", {"has-error" : errors.subcategorySquared})}>
        <label htmlFor="category" className="col-sm-3 control-label">
          Subcategory :
        </label>
        <div className="col-sm-6">
          <select className={classNames("form-control", {"select-has-error" : errors.subcategorySquared})}
            data-tip
            data-for={!!!errors.subcategorySquared || "productForm_subcategorySquared"}
            value={selectedCategory[2] || ""}
            onChange={handleInputChange.bind(null, "subcategorySquared")}>
              <option value="" hidden>Select Category</option>
              {subcategory[0].groupling.map(groupling =>
                <option key={groupling.slug}
                  value={groupling.slug}>
                    {groupling.name}
                </option>
              )}
          </select>
          <ReactTooltip id="productForm_subcategorySquared" type="error" effect="solid">
            <span>{errors.subcategorySquared}</span>
          </ReactTooltip>
        </div>
      </div>
    )
  }
}

ProductInformation.propTypes = {
  errors : PropTypes.object.isRequired,
  selectedCategory : PropTypes.string.isRequired,
  categories : PropTypes.array.isRequired,
  quantity : PropTypes.number.isRequired,
  status : PropTypes.string.isRequired,
  tags : PropTypes.array.isRequired,
  price : PropTypes.number.isRequired,
  resellCommission : PropTypes.object.isRequired,
  handleInputChange : PropTypes.func.isRequired,
  handleTagsChange : PropTypes.func.isRequired,
  handleCommissionTypeChange : PropTypes.func.isRequired
}
