"use strict";

import React, {Component} from 'react';
import ClassNames from "classnames";
import {Link} from "react-router";

import Loader from "../../../../views/Loader.jsx";

import ShopSettingsActions from "./ShopSettingsActions.js";
import ShopSettingsStores from "./ShopSettingsStores.js";

export default class ShopSettings extends React.Component {
  constructor(props) {
    super(props);
    this.state = ShopSettingsStores.getInitialState();

    this.onChange = this.onChange.bind(this);

    this.renderPaypalBanner = this.renderPaypalBanner.bind(this);
    this.renderPaypanInfo = this.renderPaypanInfo.bind(this);
    this.renderECash = this.renderECash.bind(this);

    this.handleChange = this.handleChange.bind(this);
    this.handleSavePaypal = _.throttle(this.handleSavePaypal.bind(this), 1000, {trailing : false});
    this.handleSaveECash = _.throttle(this.handleSaveECash.bind(this), 1000, {trailing : false});
  }

  componentWillMount () {
    ShopSettingsStores.addChangeListener(this.onChange);
    ShopSettingsActions.initialize();
  }

  componentWillUpdate (nextProps, nextState) {
    const {paypalInfo, eCashInfo, hasRightToSell} = nextState;
    const hasNpplQuery = this.props.location.query.nppl && nextProps.location.query.nppl;
    if (hasRightToSell && this.props.location.query.nppl) {
      this.context.router.replace({
        pathname : nextProps.location.pathname,
      });
    } else if (!hasRightToSell && !this.props.location.query.nppl) {
      this.context.router.replace({
        pathname : nextProps.location.pathname,
        query : {
          nppl : true
        }
      });
    }
  }

  componentWillUnmount () {
    ShopSettingsStores.removeChangeListener(this.onChange);
  }

  render() {
    const {initialized, error} = this.state;
    const {location} = this.props;
    if (!initialized && location.query.nppl) {
      return <div/>
    } else if (!initialized) {
      return <Loader colStyle={{paddingTop : "50px"}} />
    }

    return (
      <div>
        <h3>Shop Settings</h3>
        {this.renderECash()}
      </div>
    );
  }

  renderPaypalBanner () {
    return (
      <div className={ClassNames("alert alert-danger alert-dismissible",
        {hide : !this.props.location.query.nppl})} role="alert">
        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        Please connect your paypal account.
      </div>
    )
  }

  renderPaypanInfo () {
    const {shopName, paypalInfo, eCashInfo, initialized, error} = this.state;
    return (
      <div id="shopsettings-container">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">Paypal Information</h3>
          </div>
          <div className="panel-body">
            <div className="form-horizontal">
                <div className="form-group hide">
                  <label htmlFor="ShopName" className="col-sm-2 control-label">
                    <span>Shop Name</span>
                  </label>
                  <div className="col-sm-10">
                    <input type="text"
                      className="form-control"
                      ref="shopName"
                      id="ShopName"
                      value={shopName}
                      onChange={this.handleChange.bind(null, "shopName")}
                      placeholder="Shop Name" />
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="FirstName" className="col-sm-2 control-label">
                    <span>First Name</span>
                  </label>
                  <div className={ClassNames("col-sm-10", {"has-error" : error.firstName})}>
                    <input type="text"
                      className="form-control"
                      value={_.safe(paypalInfo, "name.firstName")}
                      onChange={this.handleChange.bind(null, "firstName")}
                      placeholder="First Name"/>
                    <label className={ClassNames("control-label", {hide : !error.firstName})}
                      htmlFor="FirstName">
                        {error.firstName}
                    </label>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="LastName" className="col-sm-2 control-label">
                    <span>Last Name</span>
                  </label>
                  <div className={ClassNames("col-sm-10", {"has-error" : error.lastName})}>
                    <input type="text"
                      className="form-control"
                      ref="lastName"
                      id="LastName"
                      value={_.safe(paypalInfo, "name.lastName")}
                      onChange={this.handleChange.bind(null, "lastName")}
                      placeholder="Last Name" />
                    <label className={ClassNames("control-label", {hide : !error.lastName})}
                      htmlFor="FirstName">
                        {error.lastName}
                    </label>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="Email" className="col-sm-2 control-label">
                    <span>Email</span>
                  </label>
                  <div className={ClassNames("col-sm-10", {"has-error" : error.emailAddress})}>
                    <input type="text"
                      className="form-control"
                      value={paypalInfo.emailAddress}
                      onChange={this.handleChange.bind(null, "emailAddress")}
                      placeholder="Email" />
                    <label className={ClassNames("control-label", {hide : !error.emailAddress})}
                      htmlFor="FirstName">
                        {error.emailAddress}
                    </label>
                  </div>
                </div>
              <button className="btn btn-primary pull-right" onClick={this.handleSavePaypal}>
                save
              </button>
            </div>
          </div>
        </div>
        <div className="modal fade"
          data-keyboard="false"
          data-backdrop="static"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="formStatusModal"
          id="formStatusModal">
          <div className="modal-dialog modal-sm">
            <div className="modal-content">
              <div className="modal-body text-center">
                <h4 className="bold f-i no-margin">
                  <span className="glyphicon glyphicon-ok m-r-s text-success"></span>
                  <span>
                    Information Updated
                  </span>
                  </h4>
              </div>
            </div>
          <Link to="/" className="close-sm-modal" data-dismiss="modal">
            Continue
          </Link>
          </div>
        </div>
      </div>
    )
  }

  renderECash () {
    const {eCashInfo, error} = this.state;

    return (
      <div id="eCash-container">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">e-Cash Information</h3>
          </div>
          <div className="panel-body">
            <div className="form-horizontal">
              <div className="form-group">
                <label htmlFor="eCashEmailUsername" className="col-sm-2 control-label">
                  <span>Username</span>
                </label>
                <div className={ClassNames("col-sm-10", {"has-error" : error.username})}>
                  <input className="form-control"
                    value={eCashInfo.username}
                    onChange={this.handleChange.bind(null, "username")}
                    placeholder="Username" />
                  <label className={ClassNames("control-label", {hide : !error.username})}
                    htmlFor="FirstName">
                      {error.username}
                  </label>
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="password" className="col-sm-2 control-label">
                  <span>Password</span>
                </label>
                <div className={ClassNames("col-sm-10", {"has-error" : error.password})}>
                  <input type="password"
                    className="form-control"
                    onChange={this.handleChange.bind(null, "password")}
                    placeholder="Password" />
                  <label className={ClassNames("control-label", {hide : !error.password})}
                    htmlFor="FirstName">
                      {error.password}
                  </label>
                </div>
              </div>
              <div className="form-group hide">
                <label className="col-sm-2 control-label">
                  <span>Merchant Id</span>
                </label>
                <div className="col-sm-10">
                  <input className="form-control"
                    value={eCashInfo.merchantId}
                    placeholder="Username"
                    disabled />
                </div>
              </div>
              <button className="btn btn-primary pull-right"
                onClick={this.handleSaveECash}>
                SAVE
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }

  onChange () {
    this.setState(ShopSettingsStores.getState());
  }

  handleChange (type, e) {
    let {paypalInfo, eCashInfo} = this.state;

    switch (type) {
      case "firstName":
        paypalInfo.name = paypalInfo.name || {};
        paypalInfo.name[type] = e.target.value.trim();
        break;
      case "lastName":
        paypalInfo.name = paypalInfo.name || {};
        paypalInfo.name[type] = e.target.value.trim();
        break;
      case "emailAddress":
        paypalInfo[type] = e.target.value.trim();
        break;
      case "username":
        eCashInfo.username = e.target.value.trim();
        break;
      case "password":
        eCashInfo.password = e.target.value.trim();
        break;
    }

    ShopSettingsActions.setInfo({
      paypalInfo,
      eCashInfo
    });
  }

  handleSavePaypal (e) {
    e.preventDefault();
    const {paypalInfo} = this.state;
    let error = {};

    if (!_.safe(paypalInfo, "name.firstName")) {
      error.firstName = "First name is required.";
    }

    if (!_.safe(paypalInfo, "name.lastName")) {
      error.lastName = "Last name is required.";
    }

    if (!paypalInfo.emailAddress) {
      error.emailAddress = "Email address is required.";
    }

    if (!_.isEmpty(error)) {
      return ShopSettingsActions.setInfo({
        error
      });
    }

    ShopSettingsActions.save({
      paypalInfo
    });
  }

  handleSaveECash (e) {
    e.preventDefault();
    const {eCashInfo} = this.state;
    let error = {};

    if (!eCashInfo.username) {
      error.username = "Username is required.";
    }

    if (!eCashInfo.password) {
      error.password = "Password is required.";
    }

    if (!_.isEmpty(error)) {
      return ShopSettingsActions.setInfo({
        error
      });
    }

    ShopSettingsActions.save({
      eCashInfo
    });
  }
}

ShopSettings.contextTypes = {
  router : React.PropTypes.object.isRequired
};
