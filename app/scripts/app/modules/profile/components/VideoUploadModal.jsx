"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM, {render} from "react-dom";

import Utils from "../../../../utils/utils.js";

export default class VideoUploadModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      videolink : false,
      media : null,
      videoFile : null,
      videoSizeMsg : "",
      intent : ""
    };

    this.onClickNewVideo = this.onClickNewVideo.bind(this);
    this.onNewVideo = this.onNewVideo.bind(this);
    this.saveVideo = this.saveVideo.bind(this);
    this.onPageReload = this.onPageReload.bind(this);
    this.onExitModal = this.onExitModal.bind(this);
    this.onAddYoutubeLink = this.onAddYoutubeLink.bind(this);
  }

  componentDidMount () {
    window.addEventListener("beforeunload", this.onPageReload);
  }

  componentWillReceiveProps (nextProps) {
    if (_.safe(nextProps, "youtubeMetadata.state") === "valid") {
      this.setState({
        intent : "youtube"
      });
    } else if (_.safe(this, "props.youtubeMetadata.doneUploading") &&
      !_.safe(nextProps, "youtubeMetadata.type.doneUploading")) {
      this.setState({
        intent : ""
      });
    }
  }

  componentWillUnmount () {
    window.removeEventListener("beforeunload", this.onPageReload);
  }

  render() {
    const {video, isOwner, videoMetadata, youtubeMetadata, modalId} = this.props;
    const {videolink, media} = this.state;

    if (!isOwner) {
      return <div/>
    }

    return (
      <div className="modal fade"
        id={modalId}
        tabIndex="-1"
        role="dialog"
        data-keyboard="false"
        data-backdrop="static"
        aria-labelledby="myModalLabel">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" aria-label="Close" onClick={this.onExitModal}>
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 className="modal-title" id="myModalLabel">Change Profile Video</h4>
            </div>
            <div className="modal-body">
              <div className="row m-b-s">
                <div className="col-sm-4">
                  <button className="btn btn-primary btn-block btn-new-vid" onClick={this.onClickNewVideo}>
                    Upload new video
                  </button>
                  <input type="file"
                    className="hidden"
                    id={`new-video-input-${modalId}`}
                    onChange={this.onNewVideo}
                    accept="video/*" />
                </div>
                <div className="col-sm-8">
                  <div className="profile-video-link-container no-margin">
                    <input className="form-control"
                      onChange={this.onAddYoutubeLink}
                      placeholder="Or provide a youtube video link here."/>
                    {this.renderYoutubeGlyphicon()}
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  {this.renderVideoByType()}
                </div>
              </div>
              <div className={"progress" + (videoMetadata.progress ? "" : " hidden")}>
                <div className="progress-bar progress-bar-success"
                  role="progressbar"
                  aria-valuenow={videoMetadata.progress}
                  aria-valuemin="0"
                  aria-valuemax="100"
                  style={{width: `${videoMetadata.progress}%`}}>
                  <span className="sr-only">{videoMetadata.progress}% Complete (success)</span>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" onClick={this.onExitModal}>Close</button>
              <button type="button" className="btn btn-primary" onClick={this.saveVideo}>Upload</button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderVideoByType () {
    const {video, youtubeMetadata} = this.props;
    const {intent, media} = this.state;
    const src = `https://www.youtube.com/embed/${youtubeMetadata.videoId}?autoplay=0&showinfo=0&controls=0`;

    if (intent === "youtube" && youtubeMetadata.state === "valid") {
      return <iframe width="100%" height={325} src={src} frameBorder="0" allowFullScreen/>
    } else if (video.resource_type === "youtube" && !intent) {
      const src = `https://www.youtube.com/embed/${video.secure_url}?autoplay=0&showinfo=0&controls=0`;
      return <iframe width="100%" height={325} src={src} frameBorder="0" allowFullScreen/>
    }

    return (
      <video className="f-w"
        muted={true}
        autoPlay={true}
        autoBuffer={true}
        loop={true}
        src={media || _.safe(video, "secure_url", config.defaultProfileVideo)}>
          Your browser doesn't support HTML5 video tag.
      </video>
    )
  }

  renderYoutubeGlyphicon () {
    const {youtubeMetadata} = this.props;

    switch (youtubeMetadata.state) {
      case "valid":
        return <span className={"glyphicon glyphicon-ok valid-link"}></span>
      case "invalid":
        return <span className={"glyphicon glyphicon-remove invalid-link"}></span>
      case "fetching":
        return <span className={"glyphicon glyphicon-refresh glyphicon-refresh-animate"}></span>
      default:
        return <span/>
    }
  }

  onClickNewVideo (e) {
    const {modalId} = this.props;
    document.getElementById(`new-video-input-${modalId}`).click();
  }

  onNewVideo (e) {
    const file = e.target.files[0];
    let isVideo = false;

    if (!file) return;
    isVideo = file.type && file.type.indexOf("video") !== -1;

    if (!file.size) {
      toastr.error("Invalid file.");
    } else if (file.size > config.uploadLimits.videoSize * 1000 * 1000) {
      toastr.warning("File size exceeds the limit of " + config.uploadLimits.videoSize + " MB.");
    } else if (isVideo) {
      this.setState({
        media : window.URL.createObjectURL(file),
        videoFile : file,
        intent : "video"
      });
    }
  }

  saveVideo () {
    const {uploadVideo, videoMetadata, youtubeMetadata, updateProfile, modalId} = this.props;
    const {videoFile, intent} = this.state;
    const mediaType = modalId === "CoverVideoModal" ? "coverVideo" : "profileVideo";

    if (videoMetadata.isUploading || (!videoFile && intent !== "youtube")) return;
    else if (intent === "youtube") {
      return updateProfile({
        metadata : {
          [mediaType] : {
            resource_type : "youtube",
            secure_url : youtubeMetadata.videoId,
            youtubeMetadata : youtubeMetadata.metadata
          }
        }
      });
    }

    uploadVideo(videoFile, progress => this.setState({progress}));
  }

  onAddYoutubeLink (e) {
    const {checkYoutubeLink, modalId} = this.props;
    const url = e.target.value.trim();
    const mediaType = modalId === "CoverVideoModal" ? "cover" : "profile";

    if (url) {
      checkYoutubeLink(url, mediaType);
    }
  }

  onPageReload (e) {
    const {isUploading} = this.props.videoMetadata;
    const msg = "You haven't finished uploading a video. Do you want to leave without finishing?";

    if (!isUploading) return;

    e.returnValue = msg;
    return msg;
  }

  onExitModal (e) {
    const {videoMetadata, modalId} = this.props;
    const msg = "You haven't finished uploading a video. Do you want to leave without finishing?";

    if (videoMetadata.isUploading && !confirm(msg)) {
      return;
    }

    $("#" + modalId).modal("hide");
  }
}

VideoUploadModal.propTypes = {
  modalId : PropTypes.string.isRequired,
  isOwner : PropTypes.bool.isRequired,
  video : PropTypes.object.isRequired,
  checkYoutubeLink : PropTypes.func.isRequired,
  uploadVideo : PropTypes.func.isRequired,
  updateProfile : PropTypes.func.isRequired,
  youtubeMetadata : PropTypes.object.isRequired,
  videoMetadata : PropTypes.object.isRequired
}
