"use strict";

import keyMirror from "keymirror";

export default keyMirror({
  SHOPSETTINGS_CHANGE_EVENT : null,
  SHOPSETTINGS_INITIALIZE : null,
  SHOPSETTINGS_SET_INFO : null,
  SHOPSETTINGS_SAVE : null
});
