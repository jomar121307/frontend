import React, {Component} from "react";
import {Link} from "react-router";

export default class NotFound extends Component {
  render () {
    return (
      <div className="container" id="notFound-container">
        <div className="row">
          <div className="col-sm-4 col-xs-12">
            <img src="./images/404.png" className="img-responsive"/>
          </div>
          <div className="col-sm-6 col-xs-12">
            <div className="notFound-desc">
            </div>
            <h2>Oops! Something went wrong.</h2>
            <p>The page you requested cannot be found.</p>
            <Link to="/">
              <button className="btn btn-home">back to home</button>
            </Link>
          </div>
        </div>
      </div>
    )
  }
}
