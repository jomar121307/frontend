"use strict";

import React, {Component} from "react";
import {render} from "react-dom";

export default class CommissionIcons extends Component {
  constructor (props) {
    super(props);
  }

  componentDidMount () {
    $("body").tooltip({
      selector : "[data-toggle=tooltip]"
    });
  }

  render () {
    let {product} = this.props;
    let resellCommission = 0;
    let tagCommission = 0;
    let hide = false;

    _.ensure(product, "resellCommission", {});
    _.ensure(product, "tagCommission", {});

    try {
      if (product.resellCommission.type === "percentage") {
        resellCommission = product.price * (product.resellCommission.value / 100);
      } else if (product.resellCommission.type === "fixed") {
        resellCommission = product.resellCommission.value;
      } else {
        resellCommission = 0;
      }

      if (product.tagCommission.type === "percentage") {
        tagCommission = product.price * (product.tagCommission.value / 100);
      } else if (product.tagCommission.type === "fixed") {
        tagCommission = product.tagCommission.value;
      } else {
        tagCommission = 0;
      }

      resellCommission = resellCommission.toFixed(2);
      tagCommission = tagCommission.toFixed(2);
    } catch (e) {
      console.error(e);

      resellCommission = 0;
      tagCommission = 0;
    }

    hide = !(resellCommission && tagCommission);

    return (
      <div className={hide ? "" : ""}>
        <i className="fa fa-money pointer commission-icon"
          data-toggle="tooltip"
          data-placement="bottom"
          title={"Tag Sale: " + tagCommission + " " + _.safe(product, "currency.code") + " " +
            "Resell: " + resellCommission + " " + _.safe(product, "currency.code")}>
        </i>
      </div>
    )
  }
}
