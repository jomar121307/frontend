import PurchasedItems from "./purchases/Purchases.jsx";
import CustomerOrders from "./orders/Orders.jsx";
import ShopSettings from "./shopsettings/ShopSettings.jsx";
import ControlPanel from "./controlpanel/ControlPanel.jsx";

const Components = {
  PurchasedItems : PurchasedItems,
  CustomerOrders : CustomerOrders,
  ShopSettings : ShopSettings,
  ControlPanel : ControlPanel
};

export default Components;
