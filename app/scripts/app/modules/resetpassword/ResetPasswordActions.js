"use strict";

import underscore from "underscore";
import lodash from "lodash"
import AppDispatcher from "../../dispatcher/AppDispatcher.js";
import ResetPasswordConstants from "./ResetPasswordConstants.js";

const ActionsNames = [
  "initialize",
  "reset",
  "processUrl", // used for multiple queries and params
  "resetPassword",
  "setForm",
  "showResetFields",
  "setPassword"
];

const Actions = {};

underscore.each(ActionsNames, name => {
  Actions[name] = function (options) {
    AppDispatcher.handleViewAction({
      type : ResetPasswordConstants["RESETPASSWORD_" + lodash.snakeCase(name).toUpperCase()],
      params : {
        ...options
      }
    });
  }
});

Actions.resetPassword = underscore.throttle(Actions.resetPassword, 1000, {trailing : false});

export default Actions;
