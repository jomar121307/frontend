"use strict";

import moment from "moment";
import Request from "./request.js";
import config from "../config.js";

const Utils = {
  dataURItoBlob : function (dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
  },

  /**
   * Extracts the videoId of a youtube link
   *
   * @method     youtubeExtractVideoId
   * @param      {string}  url     { description }
   * @return     {string}  { description_of_the_return_value }
   */
	youtubeExtractVideoId : function (url) {
    let string = "";
    let url2 = "";
    const regex = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;

    if (!url) return;

    try {
      string = decodeURIComponent(url).split(/\n| /);
      string.forEach(function (substring) {
        if (substring.match(regex)) {
          url2 = substring;
        }
      });
    } catch (e) {
      console.error(e);
    }

    return url2.match(regex) ? RegExp.$1 : "";
	},

  /**
   * Handler for infinite scrolling
   *
   * @method     lemniscateScroll
   * @param      {Function}  callback   { description }
   * @param      {boolean}   isLoading  { description }
   * @param      {float}    threshold  { (0,1) }
   */
  lemniscateScroll : function (callback, isLoading, threshold) {
    const scrollHeight = document.body.scrollHeight;
    const currentHeight = window.scrollY + document.documentElement.clientHeight;
    if ((currentHeight / scrollHeight) > threshold && !isLoading) {
      callback();
    }
  },

  deepCompare : function (a, b) {
    try {
      return JSON.stringify(a) === JSON.stringify(b);
    } catch (e) {
      console.error(e);
      return false;
    }
  },

  getHandlingFee : function (amount) {
    let fee = 0;

    if (amount < 0) {
      throw new Error("Amount is less than zero.");
    }

    fee = Math.ceil(amount * config.platformFee.percentage) / 100;
    fee += config.platformFee.flat;

    return fee;
  },

  getCategories : function (callback) {
    Request.get("misc", {
      where : JSON.stringify({name : "Product Categories"})
    }, callback);
  },

  getUser : function (callback) {
    Request.get("users/me", callback);
  },

  verifyToken : function (options, callback) {
    Request.post("verifyToken", {
      ...options
    }, callback);
  },

  /**
   * Parse dates like facebook :)
   *
   * @method     snParseDate
   * @param      {<type>}             d       { description }
   * @return     {(Function|string)}  { description_of_the_return_value }
   */
  snParseDate : function (d) {
    let date = moment(new Date(d))
      .calendar(null, {
        sameDay: '[Today]',
        lastDay: '[Yesterday] [at] hh:mm a',
        lastWeek: '[Last] dddd [at] hh:mm a',
        sameElse: 'MMMM DD [at] hh:mm a'
      });
    const createdAt = moment(new Date(d));
    const now = moment();

    if (now.diff(createdAt, "minutes") <= 2) {
      date = "Just now";
    } else if (now.diff(createdAt, "minutes") > 2 && now.diff(createdAt, "hours") < 1) {
      date = now.diff(createdAt, "minutes") + " mins";
    } else if (now.diff(createdAt, "hours") === 1 && date === "Today") {
      date = now.diff(createdAt, "hours") + " hr";
    } else if (now.diff(createdAt, "hours") >= 1 && date === "Today") {
      date = now.diff(createdAt, "hours") + " hrs";
    } else if ((now.year() - createdAt.year()) >= 1) {
      date = createdAt.format("MMMM DD YYYY [at] hh:mm a");
    }

    return date;
  },

  processState : function (args) {
    const {err, newState, state, componentName} = args;

    if (err && typeof err === "string") {
      toastr.error(err);
      console.error(err);
    } else if (_.safe(err, "xhr.responseJSON.raw.invalidAttributes")) {
      const attr = _.keys(err.xhr.responseJSON.raw.invalidAttributes);
      const errMsg = _.safe(err, "xhr.responseJSON.raw.invalidAttributes." + attr[0] + ".0.message");

      toastr.error(errMsg);
      console.error(errMsg);
    } else if (_.safe(err, "xhr.responseJSON.invalidAttributes")) {
      const attr = _.keys(err.xhr.responseJSON.invalidAttributes);
      const errMsg = _.safe(err, "xhr.responseJSON.invalidAttributes." + attr[0] + ".0.message");

      toastr.error(errMsg);
      console.error(errMsg);
    } else if (_.safe(err, "xhr.responseText")) {
      toastr.error(err.xhr.responseText);
      console.error(err);
    }

    for (var prop in newState) {
      if (state.hasOwnProperty(prop)) {
        state[prop] = newState[prop];
      } else {
        console.warn(`Invalid state for ${componentName}: ${prop}`);
      }
    }
  },
}

module.exports = Utils;
