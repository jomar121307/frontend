"use strict";

var keyMirror = require("keymirror");

module.exports = keyMirror({
  APP_CHANGE_EVENT : null,
  APP_INITIALIZE : null,
  APP_GET_NOTIFICATIONS : null,
  APP_REGISTER : null,
  APP_LOGIN : null,
  APP_LOGOUT : null,
  APP_UPDATE_USER : null,
  APP_SET_PAYPAL_INFO_FLAG : null,
  APP_SET_CATEGORIES : null
});
