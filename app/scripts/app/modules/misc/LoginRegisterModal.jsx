import React, {Component, PropTypes} from 'react';
import {Link} from "react-router";
import ClassNames from "classnames";
import Utils from "../../../utils/utils.js";

export default class LoginRegisterModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab : "login",
      type : "",
      loginErrors : {},
      signupErrors : {}
    };

    this.selectedTab = this.selectedTab.bind(this);
    this.onLogin = this.onLogin.bind(this);
    this.onRegister = this.onRegister.bind(this);
    this.onResetPassword = this.onResetPassword.bind(this);
  }

  componentWillReceiveProps (nextProps) {
    const {type} = this.state;

    if (!this.props.isLoggedIn && nextProps.isLoggedIn && type === "native") {
      this.refs["loginPassword"].value = "";
    } else if (!_.isEmpty(nextProps.signupErrors)) {
      this.setState({
        signupErrors : _.merge({}, nextProps.signupErrors)
      });
    }
  }

  render() {
    const {
      selectedTab
    } = this.state;
    return (
      <div className="modal fade"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="myModalLabel"
        id="LoginRegister-body">
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header no-pad">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
               </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-sm-8 no-pad-right-i">
                    <div id="carousel-example-generic" className="carousel slide carousel-fade" data-ride="carousel">
                      <ol className="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" className="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                      </ol>
                      <div className="carousel-inner" role="listbox">
                        <div className="item active">
                          <img src="http://res.cloudinary.com/ogagtep/image/upload/c_fill,h_338/v1459833765/cover-1_ryvne4.jpg"
                            className="crsl-img" />
                          <div className="carousel-caption">
                            <span className="fa fa-thumbs-o-up crsl-icon"></span>
                            <h2>Easy</h2>
                            <p>Time for a sharp Easy Shopping.</p>
                          </div>
                        </div>
                        <div className="item">
                          <img src="http://res.cloudinary.com/ogagtep/image/upload/c_fill,h_338/v1459833783/cover-2_sqbdo2.jpg"
                             className="crsl-img" />
                          <div className="carousel-caption">
                            <span className="fa fa-usd crsl-icon"></span>
                            <h2>Save</h2>
                            <p>Bargain, you{"'"}ve got it!</p>
                          </div>
                        </div>
                        <div className="item">
                          <img src="http://res.cloudinary.com/ogagtep/image/upload/c_fill,h_338/v1459833767/cover-3_uejjkh.jpg"
                            className="crsl-img" />
                          <div className="carousel-caption">
                            <span className="fa fa-paper-plane crsl-icon"></span>
                            <h2>Reliable</h2>
                            <p>You{"'"}re assured the product is delivered!</p>
                          </div>
                        </div>
                      </div>
                      <a className="left carousel-control"
                        href="#carousel-example-generic"
                        role="button"
                        data-slide="prev">
                          <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span className="sr-only">Previous</span>
                      </a>
                      <a className="right carousel-control"
                        href="#carousel-example-generic"
                        role="button"
                        data-slide="next">
                          <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span className="sr-only">Next</span>
                      </a>
                    </div>
                  </div>
                  <div className="col-sm-4 no-pad-left-i">
                    <ul className="nav nav-tabs">
                      <li className={ClassNames("pointer",
                        {active : selectedTab === "login"})}
                        onClick={this.selectedTab.bind(null, "login")}>
                          <a>Login</a>
                      </li>
                      <li className={ClassNames("pointer",
                        {active : selectedTab === "signup"})}
                        onClick={this.selectedTab.bind(null, "signup")}>
                          <a>Signup</a>
                      </li>
                    </ul>
                    <div className="tab-content">
                      <div className="tab-pane active">
                        {selectedTab === "login" ? this.renderLoginTab() : this.renderSignupTab()}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }

  selectedTab(tab, e){
    e.preventDefault();
    const {selectedTab} = this.state;

    if (selectedTab === tab) return;

    this.setState({
      selectedTab : tab
    });
  }

  renderLoginTab () {
    const {loginErrors} = this.state;

    return (
      <div className="p-s">
          <div className={ClassNames("row row-space form-group", {"has-error" : loginErrors.loginEmail})}>
            <div className="col-sm-12 no-pad">
              <input type="text"
                key={_.random(9999)}
                className={ClassNames("form-control", {"input-has-error" : loginErrors.loginEmail})}
                ref="loginEmail"
                placeholder="Username/Email"/>
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : loginErrors.loginPassword})}>
            <div className="col-sm-12 no-pad">
              <input type="password"
                key={_.random(9999)}
                ref="loginPassword"
                className={ClassNames("form-control", {"input-has-error" : loginErrors.loginPassword})}
                placeholder="Password"/>
            </div>
          </div>
          <div className="row row-space">
            <div className="col-sm-6 no-pad-left">
              <button className="btn btn-primary btn-block text-center bold"
                onClick={this.onLogin.bind(null, "native")}>
                  Login
              </button>
            </div>
            <div className="col-sm-6 no-pad-right">
              <button className="btn btn-primary btn-block btn-ups text-center bold"
                onClick={this.onLogin.bind(null, "ups")}>
                  Login with Ups
              </button>
            </div>
          </div>
        <div className="row row-space">
          <div className="col-sm-12 no-pad">
            <div className="strike">
               <span>OR</span>
            </div>
          </div>
        </div>
        <div className="row row-space">
          <div className="col-sm-12 no-pad">
            <button type="button"
              className="btn btn-facebook btn-block text-center bold"
              onClick={this.onLogin.bind(null, "facebook")}>
              Login with Facebook
            </button>
          </div>
        </div>
        <div className="row row-space">
          <div className="col-sm-12 no-pad text-center">
            <a className="pointer" onClick={this.onResetPassword}>
              Forgot password?
            </a>
          </div>
        </div>
        <div className="row row-space hide">
          <div className="col-sm-12 no-pad">
            <button type="button"
              className="btn btn-twitter btn-block text-center bold"
              onClick={this.onLogin.bind(null, "twitter")}>
              Login with Twitter
            </button>
          </div>
        </div>
      </div>
    );
  }

  renderSignupTab () {
    const {signupErrors} = this.state;

    return (
      <div className="p-s">
        <form onSubmit={this.onRegister} >
          <div className={ClassNames("row row-space form-group", {"has-error" : signupErrors.signupFirstName})}>
            <div className="col-sm-12 no-pad">
              <input type="text"
                maxLength="50"
                ref="signupFirstName"
                className={ClassNames("form-control", {"input-has-error" : signupErrors.signupFirstName})}
                placeholder={signupErrors.signupFirstName || "First Name"}/>
              <label className="control-label pull-left" style={{fontSize : "0.9em"}}>
              </label>
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : signupErrors.signupLastName})}>
            <div className="col-sm-12 no-pad">
              <input type="text"
                maxLength="50"
                ref="signupLastName"
                className={ClassNames("form-control", {"input-has-error" : signupErrors.signupLastName})}
                placeholder={signupErrors.signupLastName || "Last Name"} />
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : signupErrors.email})}>
            <div className="col-sm-12 no-pad">
              <input type="text"
                maxLength="50"
                ref="signupEmail"
                className={ClassNames("form-control", {"input-has-error" : signupErrors.email})}
                placeholder={signupErrors.email || "Email"} />
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : signupErrors.signupPassword})}>
            <div className="col-sm-12 no-pad">
              <input type="password"
                maxLength="50"
                ref="signupPassword"
                className={ClassNames("form-control", {"input-has-error" : signupErrors.signupPassword})}
                placeholder={signupErrors.signupPassword || "Password"} />
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : signupErrors.signupConfirmPassword})}>
            <div className="col-sm-12 no-pad">
              <input type="password"
                maxLength="50"
                ref="signupConfirmPassword"
                className={ClassNames("form-control", {"input-has-error" : signupErrors.signupConfirmPassword})}
                placeholder={signupErrors.signupConfirmPassword || "Confirm Password"} />
            </div>
          </div>
          <div className="row row-space">
            <div className="col-sm-12 no-pad">
              <p>By clicking register, you agree to our <Link to="/termsandconditions" target="_blank">Terms and Conditions</Link></p>
              <button type="submit" className="btn btn-primary btn-block text-center bold">
                  Register
              </button>
              <p className="m-t-s hide"><input type="checkbox" className="newsletter-checkbox" />Newsletter Sign Up</p>
            </div>
          </div>
        </form>
        <div className="row row-space hide">
          <div className="col-sm-12 no-pad">
            <div className="strike">
               <span>OR</span>
            </div>
          </div>
        </div>
        <div className="row row-space hide">
          <div className="col-sm-12 no-pad">
            <button type="button"
              className="btn btn-facebook btn-block text-center bold"
              onClick={this.onLogin.bind(null, "facebook")}>
              Register with Facebook
            </button>
          </div>
        </div>
        <div className="row row-space hide">
          <div className="col-sm-12 no-pad">
            <button type="button"
              className="btn btn-twitter btn-block text-center bold"
              onClick={this.onLogin.bind(null, "twitter")}>
              Register with Twitter
            </button>
          </div>
        </div>
      </div>
    );
  }

  onLogin (type, e) {
    e.preventDefault();

    const {loginUser} = this.props;
    let {loginErrors} = this.state;

    if (type === "facebook") {
      loginUser({
        type,
        userEmail : "",
        password : ""
      });
      return;
    }

    _.each(this.refs, (ref, key) => {
      const value = ref.value.trim();

      switch (key) {
        case "loginEmail":
          if (!value) {
            loginErrors.loginEmail = "Username is required.";
          } else {
            delete loginErrors.loginEmail
          }

          break;
        case "loginPassword":
          if (!value) {
            loginErrors.loginPassword = "Password is required.";
          } else {
            delete loginErrors.loginPassword;
          }

          break;
      }
    });

    if (!_.isEmpty(loginErrors)) {
      return this.setState({
        loginErrors : loginErrors
      });
    }

    this.setState({
      type : type
    });

    loginUser({
      type,
      userEmail : _.safe(this ,"refs.loginEmail.value", "").trim(),
      password : _.safe(this ,"refs.loginPassword.value", "").trim()
    });
  }

  onRegister (e) {
    e.preventDefault();

    const {registerUser} = this.props;
    let {signupErrors} = this.state;

    _.each(this.refs, (ref, key) => {
      const value = ref.value.trim();
      switch (key) {
        case "signupFirstName":
          if (!value) {
            signupErrors.signupFirstName = "First name is required.";
          } else {
            delete signupErrors.signupFirstName;
          }
          break;
        case "signupLastName":
          if (!value) {
            signupErrors.signupLastName = "Last name is required.";
          } else {
            delete signupErrors.signupLastName;
          }
          break;
        case "signupEmail":
          if (!value) {
            signupErrors.email = "Email address is required.";
          } else if (!config.emailRegex.test(value)) {
            signupErrors.email = "Email address is invalid."
            this.refs["signupEmail"].value = "";
          } else {
            delete signupErrors.email;
          }
          break;
        case "signupPassword":
          if (!value) {
            signupErrors.signupPassword = "Password is required.";
          } else if (value.length < 8) {
            signupErrors.signupPassword = "Password must be atleast 8 characters."
          } else {
            delete signupErrors.signupPassword;
          }
          break;
        case "signupConfirmPassword":
          const password = this.refs["signupPassword"].value.trim();
          if (!value) {
            signupErrors.signupConfirmPassword = "Password is required.";
          } else if (value.length < 8) {
            signupErrors.signupPassword = "Password must be atleast 8 characters."
            signupErrors.signupConfirmPassword = "Password must be atleast 8 characters."

            this.refs["signupPassword"].value = "";
            this.refs["signupConfirmPassword"].value = "";
          } else if (password !== value) {
            signupErrors.signupConfirmPassword = "Passwords don't match.";
            signupErrors.signupPassword = "Passwords don't match.";

            this.refs["signupPassword"].value = "";
            this.refs["signupConfirmPassword"].value = "";
          } else {
            delete signupErrors.signupPassword;
            delete signupErrors.signupConfirmPassword;
          }
          break;
      }
    });

    if (!_.isEmpty(signupErrors)) {
      return this.setState({
        signupErrors : signupErrors
      });
    }

    registerUser({
      firstName : this.refs["signupFirstName"].value.trim(),
      lastName : this.refs["signupLastName"].value.trim(),
      email : this.refs["signupEmail"].value.trim(),
      password : this.refs["signupPassword"].value.trim(),
      confirmPassword : this.refs["signupPassword"].value.trim()
    }, () => {
      _.each(this.refs, (r, k) => {
        r.value = "";
      });
    });
  }

  onResetPassword (e) {
    e.preventDefault();
    $("#LoginRegister-body").modal("hide");
    this.context.router.push("/resetpassword");
  }
}

LoginRegisterModal.contextTypes = {
  router : PropTypes.object.isRequired
};
