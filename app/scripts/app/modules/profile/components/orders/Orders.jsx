"use strict";

import React from "react";
import {Link} from "react-router";
import moment from "moment";
import ClassNames from "classnames";
import {ClipLoader} from "halogen";
import CopyToClipboard from 'react-copy-to-clipboard';
import Utils from "../../../../../utils/utils.js";

import Loader from "../../../../views/Loader.jsx";

import OrdersActions from "./OrdersActions.js";
import OrdersStores from "./OrdersStores.js";

export default class Orders extends React.Component {
  constructor (props) {
    super(props);
    this.state = OrdersStores.getInitialState();

    this.onChange = this.onChange.bind(this);
    this.renderSetStatus = this.renderSetStatus.bind(this);
    this.listItems = this.listItems.bind(this);
    this.handleCopy = this.handleCopy.bind(this);
  }

  componentWillMount () {
    OrdersStores.addChangeListener(this.onChange);
    OrdersActions.initialize(this.props.location.query.dogmode);
  }

  componentWillReceiveProps (nextProps) {
    OrdersActions.initialize(nextProps.location.query.dogmode);
  }

  componentDidUpdate(prevProps, prevState) {
    const {activateRefundSetup} = this.state;

    if (activateRefundSetup && !prevState.activateRefundSetup) {
      $("#configureRefund").modal("show");
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const props = Utils.deepCompare(nextProps, this.props);
    const state = Utils.deepCompare(nextState, this.state);
    return !(props && state);
  }

  componentWillUnmount () {
    OrdersStores.removeChangeListener(this.onChange);

    clearTimeout(this.usernameCopyTimeout);
  }

  render () {
    const {initialized, orders, usernameCopied} = this.state;
    const isAdmin =  _.safe(this, "props.location.query.dogmode") === "true";

    if (!initialized) {
      return <Loader colStyle={{paddingTop : "50px"}}/>
    }

    return (
      <div id="customer-order-container">
        {
          isAdmin ?
          <h4>All Orders <span className="text-danger admin-mode">(ADMIN MODE)</span></h4> :
          <h4>Orders</h4>
        }
        <p>All transaction items is/are shown below. Click the row to see the complete details and manage them.</p>
        <div className="row text-center bold" style={{padding : "10px 15px"}}>
          <div className="col-xs-2">
            Order #
          </div>
          <div className="col-xs-4">
            Ordered By
          </div>
          <div className="col-xs-4">
            Order Date
          </div>
          <div className="col-xs-2">
            Status
          </div>
        </div>
        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          {orders.map(this.listItems)}
        </div>
        <div className="modal fade bs-example-modal-md"
          id="configureRefund"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="cr-modal-title">
            <div className="modal-dialog modal-md">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 className="modal-title" id="cr-modal-title">
                    Configure Refund
                  </h4>
                </div>
                <div className="modal-body">
                  Application username:&nbsp;
                  <b>{config.paypalApiUsername}</b>
                   {
                      usernameCopied ?
                      <span className="m-l-s">Copied!</span> :
                      <CopyToClipboard text={config.paypalApiUsername}
                        onCopy={this.handleCopy}>
                          <button className="m-l-s">Copy</button>
                      </CopyToClipboard>
                    }
                  <br/>
                  <a className="pointer" onClick={this.handleRefundConfig}>
                    Click me to configure your paypal accounts for refund.
                  </a>
                </div>
                <div className="modal-footer" data-dismiss="modal">
                  <button type="button" className="btn btn-primary">Done</button>
                </div>
              </div>
            </div>
        </div>
      </div>
    )
  }

  onChange () {
    this.setState(OrdersStores.getState());
  }

  listItems (item, index) {
    const {address} = item.shippingId;
    const total = _.reduce(item.items, (acc, i) => {
      return i.netAmount + acc;
    }, 0);
    const totalShipping = _.reduce(item.items, (acc, i) => {
      return i.shippingCost + acc;
    }, 0);
    const handlingFee = Math.ceil(Utils.getHandlingFee(total) * 100) / 100;
    const na = "N/A";

    return (
      <div className="panel panel-default" key={item.id}>
        <div className="panel-heading" role="tab" id={"heading" + index}>
          <h4 className="panel-title" style={{fontSize : "13px"}}>
            <a className="collapsed"
              role="button"
              data-toggle="collapse"
              data-parent="#accordion"
              href={"#" + item.id+index}
              aria-expanded="false"
              aria-controls="collapseThree">
                <div className="row text-center">
                  <div className="col-xs-2">
                    {item.referenceId || na}
                  </div>
                  <div className="col-xs-4">
                    {_.safe(item, "userId.firstName")} {_.safe(item, "userId.lastName", "")}
                  </div>
                  <div className="col-xs-4">
                    {moment(item.createdAt).format("MMM DD, YYYY hh:mm A")}
                  </div>
                  <div className="col-xs-2">
                    {item.status.toUpperCase()}
                  </div>
                </div>
            </a>
          </h4>
        </div>
        <div id={item.id+index}
          className={ClassNames("panel-collapse collapse")}
          role="tabpanel"
          aria-labelledby={"heading" + index}>
            <div className="panel-body">
              <p>
                <strong>Order Total: </strong>
                <strong className="text-success"> ₱ {total + handlingFee}</strong>
              </p>
              <p>
                <strong>Billing Address: </strong>
                {address.streetAddress},&nbsp;
                {address.city}&nbsp;
                {address.province}&nbsp;
                {address.country}&nbsp;
                {address.zipcode}
              </p>
              <p>
                <strong>Paid thru: </strong>
                {item.paymentPlatform.toUpperCase()}
              </p>
              <p className="hide">You will receive this in 1-2 business days</p>
              <p className="text-uppercase">This order contains the following item(s):</p>
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th>Item Details</th>
                    <th className="text-right">Price</th>
                  </tr>
                </thead>
                <tbody>
                  {item.items.map(this.listProducts)}
                  <tr>
                    <td className="text-right">Shipping:</td>
                    <td className="text-right">₱ {totalShipping}</td>
                  </tr>
                  <tr>
                    <td className="text-right">Handling Fee:</td>
                    <td className="text-right">₱ {handlingFee}</td>
                  </tr>
                  <tr>
                    <td className="text-right">Grand Total:</td>
                    <td className="text-right"><strong className="text-success">₱ {total + handlingFee}</strong></td>
                  </tr>
                </tbody>
              </table>
              {this.renderSetStatus(item)}
            </div>
        </div>
      </div>
    )
  }

  renderSetStatus (order) {
    const {updatingStatus} = this.state;

    if (order.status === "refunded") {
      // return (
      //   <span>{"REFUNDED"}</span>
      // )
    } else if (_.safe(this, "props.location.query.dogmode")) {
      return <div/>
    }

    return (
      <div className="d-flex">
        <select className="btn btn-primary"
          onChange={this.handleStatus.bind(null, order.id)}
          value={order.status}>
            <option value="pending">PENDING</option>
            <option value="on hold">ON HOLD</option>
            <option value="shipped">SHIPPED</option>
            <option value="refunded">REFUNDED</option>
        </select>
        <div className={ClassNames("p-l-s", {hide : !updatingStatus})}>
          <ClipLoader color="#26A65B" size="24px" margin="4px"/>
        </div>
      </div>
    )
  }

  listProducts (item, index) {
    return (
      <tr key={index}>
        <td>
          {item.quantity} x &nbsp;
          <Link to={"/product/" + item.productId.id}>
            {item.productId.name}
          </Link>
        </td>
        <td className="text-right">₱ {item.quantity * item.productId.price}</td>
      </tr>
    )
  }

  handleStatus (orderId, e) {
    OrdersActions.setStatus(orderId, e.target.value);
  }

  handleRefundConfig (e) {
    e.preventDefault();
    window.open(config.paypalRefundConfig);
  }

  handleCopy () {
    this.usernameCopyTimeout = setTimeout(() => {
      this.setState({
        usernameCopied : false
      });
    }, 500);

    this.setState({
      usernameCopied : true
    });
  }
}
