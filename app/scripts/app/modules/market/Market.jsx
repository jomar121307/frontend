"use strict";

import React, {Component, PropTypes} from "react";
import {render} from "react-dom";
import {Link} from "react-router";
import Helmet from "react-helmet";
import classNames from "classnames";

import MarketActions from "./MarketActions.js";
import MarketConstants from "./MarketConstants.js";
import MarketStores from "./MarketStores.js";
import Utils from "../../../utils/utils.js";

//components
import Highlights from "./components/Highlights.jsx";
import ShopItem from "../misc/ShopItem.jsx";
import Loader from "../../views/Loader.jsx";

import LazyLoad from 'react-lazyload';

export default class Market extends React.Component {
  constructor (props) {
    super(props);

    this.state = MarketStores.getInitialState();
    this.onChange = this.onChange.bind(this);
    this.listProducts = this.listProducts.bind(this);
    this.searchProducts = this.searchProducts.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
  }

  componentWillMount () {
    MarketStores.addChangeListener(this.onChange);
    MarketActions.initialize({
      params : this.props.params
    });
  }

  componentWillReceiveProps (nextProps) {
    const nextParams = _.safe(nextProps, "params", {});
    const params = _.safe(this, "props.params", {});
    const nextQuery = _.safe(nextProps, "location.query", {});
    const query = _.safe(this, "props.location.query", {});

    if (!Utils.deepCompare(nextParams, params) || !Utils.deepCompare(nextQuery, query)) {
      MarketActions.initialize({
        params : nextParams,
        query : nextQuery,
        reinitialize : true
      });
    }
    // if (nextProps.params.splat) {
    //   MarketActions.initialize({
    //     splat : nextProps.params.splat,
    //     reinitialize : true
    //   });

    //   return;
    // } else if (_.isEmpty(nextProps.params) && !_.isEmpty(this.props.params)) {
    //   MarketActions.initialize({});
    // }
  }

  componentDidUpdate (prevProps, prevState) {
    const products = this.state.products;
    const prevProducts = prevState.products;

    if (!Utils.deepCompare(products, prevProducts) &&
    products.length && prevState.isSearching && !this.state.isSearching) {
      window.scroll(0,600);
    }

  }

  componentDidMount () {
    const {isLoadingMore, initialized} = this.state;
    window.onscroll = Utils.lemniscateScroll
      .bind(null, MarketActions.loadMore, isLoadingMore && !initialized, 0.80);
  }

  componentWillUnmount () {
    window.onscroll = function () {};
    MarketStores.removeChangeListener(this.onChange);
  }

  render () {
    const {
      brands,
      highlightedProducts,
      productCount,
      initialized,
      isSearching,
      isLoadingMore
    } = this.state;
    const {params} = this.props;
    let header = "";
    let {products} = this.state;
    if (params.subcategory) {
      header = params.subcategory;
    } else if (params.category) {
      header = params.category;
    }

    return (
      <div className="brand-container" id="shop-container">
        <Helmet
          title={config.appname + " - Buy Products"}
          meta={[
            { property : "og:title", content : config.appname + " - Buy Products"},
            { property : "og:site_name", content : config.appname },
            ]} />
        <div className="container">
          <div className="div-container m-bv-s" id="search-container">
            <div className="row">
              <div className="col-sm-12">
                <input className="form-control"
                  placeholder="Search Products, Brands, Categories"
                  onKeyPress={this.searchProducts}/>
              </div>
            </div>
          </div>
          <div className="m-b-m">
            <LazyLoad>
              <Highlights initialized={initialized}
                products={highlightedProducts}
                hightLightHeader={header}
                brands={brands}/>
            </LazyLoad>
          </div>
          <div className="m-b-s">
            <div className="row">
              <div className="col-sm-12 hide">
                {products.length ? this.renderBreadCrumbs() : ""}
              </div>
              <div className="col-sm-12">
                <div className="row">
                  {
                    !initialized || (initialized && isSearching) ?
                    <Loader /> :
                    products.map(this.listProducts)
                  }
                </div>
                <div className={"row text-center " + ((initialized && products.length) || !initialized ? "hide" : "")}>
                  <hr/>
                  <h4>No products found.</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderBreadCrumbs () {
    return (
      <h4>
        <span>Latest Products</span>
        <span className="pull-right breadcrumbs">
          <a href="#">
            Fashion
          </a>
          <span> / </span>
          <a href="#">
            Accessories
          </a>
          <span> / </span>
          <a href="#">
            Watch
          </a>
        </span>
      </h4>
    )
  }

  onChange () {
    this.setState(MarketStores.getState());
  }

  listProducts (product) {
    return (
      <ShopItem
        key={product.id + _.random(999999999)}
        inOwnShop={false}
        product={product}
        resellToShop={this.resellToShop}
        className="col-sm-3 col-xs-6 m-b-s" />
    );
  }

  searchProducts (e) {
    const term = e.target.value.trim();

    if (e.which === 13) {
      this.context.router.push({
        // pathname : this.props.location.pathname,
        query : {
          q : term
        }
      });
    }
  }

  onFilterChange (options) {
    this.context.router.replace(`c/${options.category}/${options.subcategory}`);
  }
}

Market.contextTypes = {
  router : PropTypes.object.isRequired,
}
