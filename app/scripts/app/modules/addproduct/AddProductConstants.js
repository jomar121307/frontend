"use strict";

import keyMirror from "keymirror";

export default keyMirror({
  ADDPRODUCT_CHANGE_EVENT : null,
  ADDPRODUCT_INITIALIZE : null,
  ADDPRODUCT_RESET : null,
  ADDPRODUCT_SET_PRODUCT : null,
  ADDPRODUCT_SET_FORM_ERROR : null,
  ADDPRODUCT_SET_SHIPPING_TYPE : null,
  ADDPRODUCT_SET_COMMISSION_TYPE : null,
  ADDPRODUCT_AGREE_TO_TAC : null,
  ADDPRODUCT_CHECK_NAME : null,
  ADDPRODUCT_FINISH : null
});
