"use strict";

import AppDispatcher from "../../dispatcher/AppDispatcher.js";
import PaymentConstants from "./PaymentConstants.js";
import {throttle} from "underscore";

const PaymentActions = {
  initialize : function () {
    AppDispatcher.handleViewAction({
      type : PaymentConstants.PAYMENT_INITIALIZE
    });
  },

  reset : function () {
    AppDispatcher.handleViewAction({
      type : PaymentConstants.PAYMENT_RESET
    });
  },

  pay : throttle(function (params) {
    AppDispatcher.handleViewAction({
      type : PaymentConstants.PAYMENT_PAY,
      params : {
        ...params
      }
    });
  }, 5000)
}

export default PaymentActions;
