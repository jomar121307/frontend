"use strict";

import React, {Component, PropTypes} from "react";
import Utils from "../../../utils/utils.js";

export default class Cropper extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  componentDidMount () {
    const {id, options, photo} = this.props;
    $(`#${id}`).cropit(options);
    $(`#${id}`).cropit("imageSrc", photo);
  }

  componentWillUpdate (nextProps, nextState) {
    const {photo, id} = nextProps;

    if (photo) {
      $(`#${id}`).cropit("imageSrc", photo);
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    return !Utils.deepCompare(nextProps, this.props);
  }

  render () {
    const {id, fileInputId, onPhotoChange} = this.props;

    return (
      <div id={id}>
        <div className="cropit-preview"></div>
        <input type="range" className="cropit-image-zoom-input m-t-s" />
        <input type="file"
          className="cropit-image-input"
          id={fileInputId}
          onChange={onPhotoChange} />
      </div>
    )
  }
}
