"use strict";

import {EventEmitter} from "events";
import _ from "underscore";
import async from "async";

import Request from "../../../utils/request.js";
import Socket from "../../../utils/socket.js";
import {register} from "../../dispatcher/AppDispatcher.js";

import MessagesConstants from "./MessagesConstants.js";

const initialState = {
  initialized : false,
  isLoading : false,
  threads : [],
  selectedThread : {},
  roomPage : 0,
  roomSize : 10,
  threadPage : 0,
  threadSize : 50
};

let state = _.assign({}, initialState);

let Stores = _.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(MessagesConstants.MESSAGES_CHANGE_EVENTS, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(MessagesConstants.MESSAGES_CHANGE_EVENTS, callback);
  },

  emitChange : function () {
    Stores.emit(MessagesConstants.MESSAGES_CHANGE_EVENTS);
  },

  getState : function () {
    return state;
  },

  getInitialState : function () {
    return initialState;
  }
});

const Actions = {
  [MessagesConstants.MESSAGES_INITIALIZE] : function (params) {
    messagesInitialize(params, function (err, data) {
      if (err) {
        return messagesSetState(err, {
          initialized : true
        });
      }

      const threads = data.threads;
      const selectedThread = data.threads[0] || {};

      messagesSetState(null, {
        initialized : true,
        threads : data.threads,
        selectedThread : selectedThread
      });

      if (state.selectedThread.unreadCount) {
        messagesMarkRead({
          id : state.selectedThread.id
        }, function (err, data) {
          if (err) {
            return console.error(err);
          }

        });
      }
    });
  },

  [MessagesConstants.MESSAGES_UPDATE_THREADS] : function (params) {
    messagesUpdateThreads(function (err, data) {
      if (err) {
        return console.error(err);
      }

      const idx = _.findIndex(data, {
        id : state.selectedThread.id
      });

      messagesSetState(null, {
        threads : data,
        selectedThread : data[idx]
      });

      if (state.selectedThread.unreadCount) {
        messagesMarkRead({
          id : state.selectedThread.id
        }, function (err, data1) {
          if (err) {
            return console.error(err);
          }

        });
      }
    });
  },

  [MessagesConstants.MESSAGES_SELECT_THREAD] : function (params) {
    const idx = _.findIndex(state.threads, {id : params.threadId});

    if (idx === -1) {
      return;
    }

    messagesSetState(null, {
      selectedThread : _.assign({}, state.threads[idx])
    });

    if (state.selectedThread.unreadCount) {
      messagesMarkRead({
        id : state.selectedThread.id
      }, function (err, data) {
        if (err) {
          return console.error(err);
        }
      });
    }
  },

  [MessagesConstants.MESSAGES_SEND] : function (params) {
    if (!params.message) {
      return;
    }

    messagesSend(params, function (err, data) {
      if (err) {
        return messagesSetState(err, {});
      }
      const newState = $.extend(true, {}, state);
      const idx = _.findIndex(newState.threads, {id : data.roomId});

      if (idx === -1) {
        return;
      }

      newState.threads[idx].messages.push(data);
      messagesSetState(null, {
        threads : newState.threads,
        selectedThread : newState.threads[idx]
      });
    });
  },

  [MessagesConstants.MESSAGES_LOAD_MORE_MESSAGE] : function (params) {
    if (state.selectedThread.messages.length < (state.threadPage + 1) * state.threadSize) {
      return;
    }

    messagesSetState(null, {
      isLoading : true,
    });

    messagesLoadMore(function (err, data) {
      if (err) {
        return messagesSetState(err, {
          isLoading : false,
        });
      } else if (!_.safe(data, "messages.length")) {
        messagesSetState(null, {
          isLoading : false,
        });
        return;
      }

      const idx = _.findIndex(state.threads, {
        id : data.id
      });
      const newState = _.assign({}, state);
      const selectedThread = _.assign({}, state.selectedThread);

      if (idx === -1) {
        console.log("thread not found.");
        return;
      }

      selectedThread.messages = data.messages.concat(selectedThread.messages);
      state.threads[idx] = selectedThread;
      messagesSetState(null, {
        isLoading : false,
        selectedThread : selectedThread,
        threads : state.threads,
        threadPage : ++state.threadPage
      });
    });
  }
};

Stores.dispatchIndex = register(payload => {
  const {type, params} = payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function messagesSetState(err, newState) {
  if (err) {
    toastr.error(err.xhr.responseText);
    console.error(err);
  }

  for (var prop in newState) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = newState[prop];
    } else {
      console.warn("Invalid state for messages: " + prop);
    }
  }

  Stores.emitChange(MessagesConstants.MESSAGES_CHANGE_EVENTS);
}

function messagesInitialize (options, callback) {
  async.parallel({
    threads : function (parallelCb) {
      Request.get("rooms", {
        skip : state.threadSize * state.threadPage,
        limit : state.threadSize,
        sort : "updatedAt DESC"
      }, parallelCb);
    },

    user : function (parallelCb) {
      if (!options.userId) {
        return async.nextTick(parallelCb);
      }

      Request.get(`messages/initiate/${options.userId}`, parallelCb);
    }
  }, callback);
}

function messagesSend (options, callback) {
  const {selectedThread} = state;
  const to = !_.safe(selectedThread, "users.0.__self") &&
    selectedThread.users[0].id || selectedThread.users[1].id;

  Request.post(`message/${to}`, {
    roomId : selectedThread.id,
    payload : {
      message : options.message
    }
  }, callback);
}

function messagesMarkRead (options, callback) {
  Request.get(`rooms/markasread/${options.id}`, callback);
}

function messagesUpdateThreads (callback) {
  Request.get("rooms", {
    skip : state.threadSize * state.threadPage,
    limit : state.threadSize,
    sort : "updatedAt DESC"
  }, callback);
}

function messagesLoadMore (callback) {
  Request.get(`rooms/${state.selectedThread.id}`, {
    skip : (state.threadPage + 1) * state.threadSize,
    limit : state.threadSize
  }, callback);
}
