"use strict";

import keyMirror from "keymirror";

export default keyMirror({
  PRODUCT_CHANGE_EVENT : null,
  PRODUCT_INITIALIZE : null,
  PRODUCT_RESET : null,
  PRODUCT_UPLOAD_REVIEW : null,
  PRODUCT_REMOVE_REVIEW : null,
  PRODUCT_ADD_TO_CART : null
});
