"use strict";

import React, {Component, PropTypes} from 'react';
import Utils from "../../../../utils/utils.js";

export default class ShippingInformation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialized : false,
      shippingData : []
    }

    this.listShippingData = this.listShippingData.bind(this);
    this.handleAddLocation = this.handleAddLocation.bind(this);
    this.handleRemoveLocation = this.handleRemoveLocation.bind(this);
    this.handleShippingCost = this.handleShippingCost.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
  }

  componentWillReceiveProps (nextProps) {
    const {shippingData, initialized} = this.state;

    if (!shippingData.length && !initialized) {
      this.setState({
        initialized : true,
        shippingData : nextProps.shippingData
      });
    } else if (nextProps.shippingData && Utils.deepCompare(shippingData, nextProps.shippingData)) {
      this.setState({
        shippingData : nextProps.shippingData
      });
    }
  }

  render() {
    const {shippingData} = this.props;

    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          <div className="row">
            <div className="col-xs-4">
              <p className="no-margin bold">Ships To</p>
            </div>
            <div className="col-xs-4">
              <p className="no-margin bold">Cost</p>
            </div>
            <div className="col-xs-2">
            </div>
          </div>
        </div>
        <div className="panel-body" id="choose-shipping-container">
          <ul className="list-unstyled">
            <li>
              <div className="row">
                <div className="col-xs-4">
                  <p>Anywhere in the Philippines</p>
                </div>
                <div className="col-xs-4">
                  <div className="input-group">
                    <input type="number"
                      className="form-control"
                      aria-describedby="single-anywhere"
                      value={shippingData[0].singleCost}
                      onChange={this.handleShippingCost.bind(null, 0)}/>
                    <span className="input-group-addon" id="single-anywhere">PHP</span>
                  </div>
                </div>
                <div className="col-xs-2">
                </div>
              </div>
            </li>
            {shippingData.slice(1).map(this.listShippingData)}
          </ul>
          <a className="pointer" onClick={this.handleAddLocation}>
            Add a region
          </a>
        </div>
      </div>
    );
  }

  listShippingData (data, index) {
    const {provinces} = this.props;
    index++;

    return (
      <li key={index}>
        <div className="row">
          <div className="col-xs-4">
            <select className="form-control"
              placeholder="Region"
              value={data.location}
              onChange={this.handleLocationChange.bind(null, index)}>
                {provinces.map(p => <option key={p} value={p}>{p}</option>)}
            </select>
          </div>
          <div className="col-xs-4">
            <div className="input-group">
              <input type="number"
                className="form-control"
                aria-describedby={"single-" + index}
                value={data.singleCost}
                onChange={this.handleShippingCost.bind(null, index)}/>
              <span className="input-group-addon" id={"single-" + index}>PHP</span>
            </div>
          </div>
          <div className="col-xs-2">
            <span className="glyphicon glyphicon-remove pointer"
              onClick={this.handleRemoveLocation.bind(null, index)} />
          </div>
        </div>
      </li>
    )
  }

  handleAddLocation (e) {
    e.preventDefault();
    const {onAddLocation} = this.props;
    let {shippingData} = this.state;
    let {provinces} = this.props;

    if (shippingData.length >= provinces.length) {
      return;
    }

    shippingData.push({
      location : "Abra",
      singleCost : 0,
      multipleCost : 0
    });

    onAddLocation(shippingData);
  }

  handleRemoveLocation (index, e) {
    let {shippingData} = this.state;
    const {onAddLocation} = this.props;

    if (!shippingData.length) {
      return;
    }

    shippingData.splice(index, 1);
    onAddLocation(shippingData);
  }

  handleShippingCost (index, e) {
    let {shippingData} = this.state;
    const {onAddLocation} = this.props;

    shippingData[index].singleCost = e.target.value;
    onAddLocation(shippingData);
  }

  handleLocationChange (index, e) {
    let {shippingData} = this.state;
    const {onAddLocation} = this.props;

    shippingData[index].location = e.target.value;
    onAddLocation(shippingData);
  }
}

React.propTypes = {
  shippingData : PropTypes.array.isRequired,
  onAddLocation : PropTypes.func.isRequired
}
