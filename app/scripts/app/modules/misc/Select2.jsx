"use strict";

var React = require("react");

var Select2 = React.createClass({
  componentDidMount : function () {
    var self = this,
      {params, id} = this.props;
    $("#" + id)
      .select2(params)
      .on("change", function (e) {
        if (_.isFunction(self.props.onChangeSelect)) {
          self.props.onChangeSelect(id);
        }
      });
  },

  shouldComponentUpdate : function (nextProps) {
    if (_.isEqual(this.props, nextProps)) {
      return false;
    }
    // return !nextProps.isUploading;
    return true;
  },

  render : function () {
    var {options, values, multiple, required, id} = this.props;
    if (!options) {
      options = values;
    }

    return (
      <div>
        <select
          id={id}
          defaultValue={values}
          multiple={multiple}
          required={required}>
          {options ? options.map(this._listOptions) : ""}
        </select>
      </div>
    )
  },

  _listOptions : function (opt) {
    if (typeof opt === "object") {
      return (
        <option
          key={_.random(0,9999) + "-" + opt.value}
          value={opt.value}>
            {opt.text}
        </option>
      )
    }

    return (
      <option
        key={_.random(0,9999) + "-" + opt}
        value={opt}>
          {opt}
      </option>
    )
  },
});

module.exports = Select2;
