"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from "react-router";
import Helmet from "react-helmet";
import ClassNames from "classnames";

import Utils from "../../../utils/utils.js";

import Cover from "./components/Cover.jsx";
import User from "./components/User.jsx";
import ProfileProducts from "./components/ProfileProducts.jsx";

import ProfileActions from "./ProfileActions.js";
import ProfileStores from "./ProfileStores.js";

export default class Profile extends Component {
  constructor (props) {
    super(props);

    this.state = ProfileStores.getInitialState();
    this.onChange = this.onChange.bind(this);
    this.selectedTab = this.selectedTab.bind(this);
    this.renderTabs = this.renderTabs.bind(this);
    this.renderAllOrdersTab = this.renderAllOrdersTab.bind(this);
  }

  componentWillMount () {
    const {params} = this.props;

    ProfileStores.addChangeListener(this.onChange);

    if (params.id) {
      const shopVisitTracker = JSON.parse(localStorage.__svt || "[]");
      shopVisitTracker.push(params.id);
      localStorage.__svt = JSON.stringify(shopVisitTracker);
      return ProfileActions.initialize({
        shopId : this.props.params.id
      });
    } else if (this.props.location.pathname === "/shop") {
      return this.context.router.replace("/myshop");
    }

    ProfileActions.initialize();
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.params.id) {
      return ProfileActions.initialize({
        shopId : nextProps.params.id
      });
    } else if (nextProps.location.pathname === "/shop") {
      ProfileActions.initialize();
      return this.context.router.replace("/myshop");
    } else if (nextProps.location.pathname === "/myshop") {
      ProfileActions.initialize();
    }
  }

  componentWillUpdate(nextProps, nextState) {
    const {params} = this.props;

    if (_.isEmpty(this.state.user) &&
      !_.isEmpty(nextState.user) &&
      nextState.user.__self &&
      this.props.route.path === "shop/:id") {
        this.context.router.replace("/myshop");
    }
  }

  componentDidMount () {
    const {isLoadingMore, initialized} = this.props;
    window.onscroll = Utils.lemniscateScroll
      .bind(null, ProfileActions.loadMore, isLoadingMore && !initialized, 0.8);
  }

  componentWillUnmount () {
    ProfileActions.reset();
    window.onscroll = function () {};
    ProfileStores.removeChangeListener(this.onChange);
  }

  render () {
    const {
      user,
      profileVideoMetadata,
      coverVideoMetadata,
      youtubeMetadata,
      userInfoError,
      products,
      productCount,
      initialized,
      currentTab,
      hasPaypalInfo,
    } = this.state;
    let helmetTitle = config.appname + " - " + _.safe(user, "firstName", "") + " " + _.safe(user, "lastName", "");

    if (profileVideoMetadata.progress) {
      helmetTitle = `(${~~(profileVideoMetadata.progress)}%) Uploading video...`
    } else if (coverVideoMetadata.progress) {
      helmetTitle = `(${~~(coverVideoMetadata.progress)}%) Uploading video...`
    }

    return (
      <div className="brand-container">
        <Helmet title={helmetTitle}
          meta={[
          {
            property : "og:title",
            content : config.appname + " - " + _.safe(user, "firstName", "") + " " + _.safe(user, "lastName", "")
          },
          {
            property : "og:site_name",
            content : config.appname
          }
          ]} />
        <div className="container m-t-s">
          <div className="row">
            <div className="col-sm-12"  id="profile-body">
              <div className="row">
                <div className="col-xs-12 col-sm-3">
                  <User user={user}
                    hasPaypalInfo={hasPaypalInfo}
                    profileVideoMetadata={profileVideoMetadata}
                    youtubeMetadata={youtubeMetadata}
                    handleSend={ProfileActions.messageUser}/>
                  {this.renderTabs(user.__self)}
                </div>
                <div className="col-xs-12 col-sm-9">
                  {this.props.children || this.renderProductsTab()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  onChange () {
    this.setState(ProfileStores.getState());
  }

  renderTabs (isOwner) {
    const {location} = this.props;
    if (!isOwner) {
      return <div/>
    }

    return (
      <div className="profile-side-tab">
        <ul className="list-group">
          <li className={ClassNames("list-group-item", {active : location.pathname === "/myshop"})}
            onClick={this.selectedTab.bind(null, "/myshop")}>
            <span className="badge hide">
            </span>
            Products
          </li>
          <li className={ClassNames("list-group-item", {active : location.pathname === "/myshop/purchases"})}
            onClick={this.selectedTab.bind(null , "/myshop/purchases")}>
            <span className="badge hide">
            </span>
            Purchased Items
          </li>
          <li className={ClassNames("list-group-item",
            {active : location.pathname === "/myshop/orders" && !location.query.dogmode})}
            onClick={this.selectedTab.bind(null , "/myshop/orders")}>
            <span className={ClassNames("badge hide", {hide : !this.context.notifications.shop})}>
              {this.context.notifications.shop}
            </span>
            Customer Orders
          </li>
          <li className={ClassNames("list-group-item", {active : location.pathname === "/myshop/shopsettings"})}
            onClick={this.selectedTab.bind(null , "/myshop/shopsettings")}>
            Shop Settings
          </li>
          {this.renderAllOrdersTab()}
          {this.renderAddCategoriesTab()}
        </ul>
      </div>
    )
  }

  renderAllOrdersTab () {
    const {user} = this.state;
    const {location} = this.props;
    const isActive = location.pathname === "/myshop/orders" && !!location.query.dogmode

    if (!user.isAdmin) {
      return <div/>
    }

    return (
      <li className={ClassNames("list-group-item", {active : isActive})}
        onClick={this.selectedTab.bind(null , "/myshop/orders?dogmode=true")}>
        All Orders
      </li>
    )
  }

  renderAddCategoriesTab () {
    const {user} = this.state;
    const {location} = this.props;
    const isActive = location.pathname === "/myshop/controlpanel" && !!location.query.dogmode

    if (!user.isAdmin) {
      return <div/>
    }

    return (
      <li className={ClassNames("list-group-item", {active : isActive})}
        onClick={this.selectedTab.bind(null , "/myshop/controlpanel?dogmode=true")}>
        Control Panel
      </li>
    )
  }

  selectedTab (tab, e) {
    const {isLoadingMore, initialized} = this.props;

    if (tab === this.props.location.pathname && _.isEmpty(this.props.location.query)) {
      return;
    } else if (tab === "/myshop") {
      window.onscroll = Utils.lemniscateScroll
      .bind(null, ProfileActions.loadMore, isLoadingMore && !initialized, 0.90);

      return this.context.router.push(tab)
    }

    window.onscroll = function () {};
    ProfileActions.resetPagination();
    this.context.router.push(tab)
  }

  renderProductsTab () {
    const {
      user,
      coverVideoMetadata,
      youtubeMetadata,
      products,
      productCount,
      initialized,
    } = this.state;

    return (
      <div>
        <Cover user={user}
          isOwner={!!!this.props.params.id}
          coverVideoMetadata={coverVideoMetadata}
          youtubeMetadata={youtubeMetadata}/>
        <div className="row">
          <div className="col-xs-12 col-sm-12">
            <ProfileProducts products={products}
              productCount={productCount}
              initialized={initialized}/>
          </div>
        </div>
      </div>
    );
  }
}

Profile.contextTypes = {
  router : PropTypes.object.isRequired,
  notifications : PropTypes.object.isRequired
}
