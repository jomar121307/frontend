"use strict";

import AppDispatcher from "../../dispatcher/AppDispatcher.js";
import CartConstants from "./CartConstants.js";

const CartActions = {
  initialize : function () {
    AppDispatcher.handleViewAction({
      type : CartConstants.CART_INITIALIZE
    });
  },

  reset : function () {
    AppDispatcher.handleViewAction({
      type : CartConstants.CART_RESET
    });
  },

  changeItemShippingLocation : function (index, location) {
    AppDispatcher.handleViewAction({
      type : CartConstants.CART_CHANGE_SHIPPING_LOCATION,
      params : {
        index,
        location
      }
    });
  },

  updateItem : function (groupIndex, index, quantity) {
    AppDispatcher.handleViewAction({
      type : CartConstants.CART_UPDATE_QUANTITY,
      params : {
        quantity,
        index,
        groupIndex
      }
    });
  },

  removeItem : function (groupIndex, index) {
    AppDispatcher.handleViewAction({
      type : CartConstants.CART_REMOVE_ITEM,
      params : {
        groupIndex,
        index
      }
    });
  },

  proceed : function (params) {
    AppDispatcher.handleViewAction({
      type : CartConstants.CART_PROCEED,
      params : {
        ...params
      }
    });
  }
}

export default CartActions;
