import React from 'react';

export default class ContactOwner extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
        <div className="modal fade" id="contactOwner" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="myModalLabel"><span className="message-to">Message to</span> Juston Paul Alcantara</h4>
              </div>
              <div className="modal-body">
                <div className="media">
                  <div className="media-left media-top">
                    <a href="#">
                      <img className="media-object" src="" alt="..." />
                    </a>
                  </div>
                  <div className="media-body">
                    <textarea className="form-control contact-owner-textarea" placeholder="Your message here."></textarea>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Send Message</button>
              </div>
            </div>
          </div>
        </div>
        );
    }
}
