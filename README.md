# README #


* This repository is for the frontend web application of Bentanayan

* [ReactJS](https://facebook.github.io/flux/)

### How do I get set up? ###

* To setup the environment
*  Go to root folder
*  Type "npm install"
*  Type "bower install"
*  For running locally type "grunt serve"

### To build for production/staging server ###
* type "grunt build"
* Go to dist folder and copy contents for frontend


### Deployment instructions ###
*  Create a Sailsjs project. (refer to 
a [Sails](http://sailsjs.org) application
[How to get started with sailsjs](http://sailsjs.org/get-started))
*  Paste dist folder contents to assets folder in sails project(refer To build for production/staging server)
*  Duplicate app.js
*  Change the duplicated app.js to server.js
*  Create a repository that has a Sails Project in it.
*  Add repository link in the AWS Opsworks.
*  Follow bentanayan-api deployment.