"use strict";

import React, {Component, PropTypes} from "react";
import {render} from "react-dom";
import {Link} from "react-router";
import moment from "moment";
import Utils from "../../../../utils/utils.js";

export default class Reviews extends Component {
  constructor (props) {
    super(props);

    this.renderNoReviews = this.renderNoReviews.bind(this);
    this.renderForm = this.renderForm.bind(this);
    this.listReviews = this.listReviews.bind(this);
    this.handleReviewSubmit = this.handleReviewSubmit.bind(this);
  }

  render () {
    const {reviews} = this.props;

    return (
      <div className="reviews-container">
        <h4 className={reviews.length ? "" : "hidden"}>Customer Reviews</h4>
        {reviews.map(this.listReviews)}
        {this.renderNoReviews()}
        {this.renderForm()}

      </div>
    )
  }

  renderNoReviews () {
    const {reviews} = this.props;

    if (!reviews.length) {
      return (
        <h5>There are no reviews yet.</h5>
      )
    }

    return <div/>
  }

  renderForm () {
    const {isLoggedIn} = this.context;
    const {isReviewed} = this.props;

    if (!isLoggedIn || isReviewed) {
      return <div/>
    }

    return (
      <form onSubmit={this.handleReviewSubmit}>
        <textarea className="form-control non-resize review-ta"
          ref="review-content-textarea"
          required={true}>
        </textarea>
        <button className="btn btn-success btn-pg-green pull-right m-t-s"
          type="submit">
            Submit
        </button>
      </form>
    )
  }

  listReviews (review) {
    const user = review.userId;
    let thumb = _.safe(user, "metadata.profileVideo.secure_url", config.defaultYoutubeProfileVideo);
    if (_.safe(user, "metadata.profileVideo.resource_type", "youtube") === "youtube") {
      thumb = cloudinary.url(`https://i.ytimg.com/vi/${thumb}/default.jpg`, {
        height: 45,
        width: 45,
        crop: "fill",
        type : "fetch"
      });
    } else if (_.safe(user, "metadata.profileVideo.resource_type") === "video") {
      const profileVideo = user.metadata.profileVideo;

      thumb = cloudinary.url(`${profileVideo.public_id}.jpg`, {
        height: 45,
        transformation: ["media_lib_thumb"],
        width: 45,
        crop: "fill",
        resource_type: "video",
        secure : true
      });
    }

    return (
      <div key={review.id} className="media m-b-s no-m-t">
        <div className="media-left">
          <Link to={"/profile/" + _.safe(review, "userId.id", "")}>
            <img className="media-object comment-thumbnail"
              src={thumb}
              alt="..."/>
          </Link>
        </div>
        <div className="media-body">
          <h5 className="media-heading">
            {_.safe(review, "userId.firstName", "") + " " + _.safe(review, "userId.lastName", "")} •
            <span className="product-review-time-posted"> {Utils.snParseDate(review.createdAt)}</span>
          </h5>
          <p>{review.content}</p>
        </div>
      </div>
    )
  }

  handleReviewSubmit (e) {
    e.preventDefault();

    if (!this.context.isLoggedIn) {
      return;
    }

    const review = this.refs["review-content-textarea"].value.trim();
    const {uploadReview} = this.props;

    uploadReview(review);
  }
}

Reviews.propTypes = {
  uploadReview : PropTypes.func.isRequired,
  reviews : PropTypes.array.isRequired
};

Reviews.contextTypes = {
  isLoggedIn : PropTypes.bool.isRequired
}
