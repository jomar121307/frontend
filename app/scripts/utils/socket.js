/* global config */
"use strict";
let sailsSocket = null;

var Socket = {
  connect : function () {
    io.sails.headers = {
      "Authorization" : "Bearer " + localStorage.token,
      'X-App' : config.appType
    };

    sailsSocket = io.sails.connect(config.apiServer);
  },

  get : function (url, data = {}, cb = new Error("callback is not a function")) {
    sailsSocket.get(url, data, cb);
  },

  on : function (model, cb = new Error("callback is not a function")) {
    sailsSocket.on(model, cb);
  },

  post : function (url, data = new Error("data is undefined")) {
    sailsSocket.post(url, data);
  }
};

module.exports = Socket;
