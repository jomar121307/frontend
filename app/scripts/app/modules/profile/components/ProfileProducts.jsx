"use strict";

import React from "react";
import {Link} from "react-router";
import {render} from "react-dom";
import Loader from 'halogen/PulseLoader';

//Components
import ShopItem from "../../misc/ShopItem.jsx";
export default class ProfileProducts extends React.Component {
  constructor (props) {
    super(props);

    this.listProducts = this.listProducts.bind(this);
  }

  render () {
    const {
      initialized,
      products,
      productCount
    } = this.props;

    return (
      <div className="profile-product-content">
        <div className="row profile-video-container">
          {
            !initialized ?
              <div className="col-sm-12 video-content"></div>
            :
              <div>
                <h4>
                  <span>Products</span>
                </h4>
                <hr/>
                {products.map(this.listProducts)}
              </div>
          }
        </div>
        <div className={"row text-center " + ((!initialized || productCount) ? "hide" : "")}>
          No products found.
        </div>
      </div>
    )
  }

  listProducts (product) {
    return (
      <ShopItem
        key={product.id}
        inOwnShop={false}
        product={product}
        resellToShop={this.resellToShop}
        className="col-sm-4 col-xs-6 m-b-s"/>
    );
  }

  resellToShop (id) {

  }
}
