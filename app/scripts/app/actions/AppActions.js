"use strict";

var AppDispatcher = require("../dispatcher/AppDispatcher.js");
var AppConstants = require("../constants/AppConstants.js");

var AppActions = {
  initialize : function () {
    AppDispatcher.handleViewAction({
      type : AppConstants.APP_INITIALIZE,
    });
  },

  getNotifications : function () {
    AppDispatcher.handleViewAction({
      type : AppConstants.APP_GET_NOTIFICATIONS
    });
  },

  register : function (user, callback) {
    AppDispatcher.handleViewAction({
      type : AppConstants.APP_REGISTER,
      params : {
        user,
        callback
      }
    });
  },

  login : function (options, callback) {
    AppDispatcher.handleViewAction({
      type : AppConstants.APP_LOGIN,
      params : {
        options,
        callback
      }
    });
  },

  logout : function (callback) {
    AppDispatcher.handleViewAction({
      type : AppConstants.APP_LOGOUT,
      params : {
        callback
      }
    });
  },

  updateUser : function (user) {
    AppDispatcher.handleViewAction({
      type : AppConstants.APP_UPDATE_USER,
      params : {
        user
      }
    });
  },

  setPaypalInfoFlag : function (hasInfo) {
    AppDispatcher.handleViewAction({
      type : AppConstants.APP_SET_PAYPAL_INFO_FLAG,
      params : {
        hasInfo
      }
    });
  },

  setCategories : function (categories) {
    AppDispatcher.handleViewAction({
      type : AppConstants.APP_SET_CATEGORIES,
      params : {
        categories
      }
    });
  }
};

module.exports = AppActions;
