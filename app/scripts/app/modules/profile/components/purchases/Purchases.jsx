"use strict";

import React, {Component} from "react";
import {Link} from "react-router";
import moment from "moment";
import ClassNames from "classnames";
import Utils from "../../../../../utils/utils.js";

import Loader from "../../../../views/Loader.jsx";

import PurchasesActions from "./PurchasesActions.js";
import PurchasesStores from "./PurchasesStores.js";

export default class Purchases extends Component {
  constructor (props) {
    super(props);
    this.state = PurchasesStores.getInitialState();

    this.onChange = this.onChange.bind(this);
    this.listItems = this.listItems.bind(this);
  }

  componentWillMount () {
    PurchasesStores.addChangeListener(this.onChange);
    PurchasesActions.initialize();
  }

  componentWillUnmount () {
    PurchasesStores.removeChangeListener(this.onChange);
  }

  render() {
    const {initialized, purchases} = this.state;

    if (!initialized) {
      return <Loader colStyle={{paddingTop : "50px"}}/>
    }

    return (
      <div>
        <div className="alert alert-success alert-dismissible hide" role="alert">
          <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          You have successfully purchased Item #1
        </div>
        <div id="purchased-item-container">
          <h4>Purchased Items</h4>
          <p>The status of your recent orders is shown below. Click the row to see the complete details.</p>
          <div className="row text-center bold" style={{padding : "10px 15px"}}>
            <div className="col-xs-2">
              Order #
            </div>
            <div className="col-xs-4">
              Purchased from
            </div>
            <div className="col-xs-4">
              Puchase Date
            </div>
            <div className="col-xs-2">
              Status
            </div>
          </div>
          <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            {purchases.map(this.listItems)}
          </div>
        </div>
      </div>
    )
  }

  onChange () {
    this.setState(PurchasesStores.getState());
  }

  listItems (item, index) {
    const {address} = item.shippingId;
    const total = _.reduce(item.items, (acc, i) => {
      return _.safe(i, "computation.netAmount", 0) + acc;
    }, 0);
    const totalShipping = _.reduce(item.items, (acc, i) => {
      return _.safe(i, "computation.shippingCost", 0) + acc;
    }, 0);
    const handlingFee = Math.ceil(Utils.getHandlingFee(total) * 100) / 100;
    const na = "N/A";

    return (
      <div className="panel panel-default" key={item.id}>
        <div className="panel-heading" role="tab" id={"heading" + index}>
          <h4 className="panel-title" style={{fontSize : "13px"}}>
            <a className="collapsed"
              role="button"
              data-toggle="collapse"
              data-parent="#accordion"
              href={"#" + item.id + index}
              aria-expanded="false"
              aria-controls="collapseThree">
                <div className="row text-center">
                  <div className="col-xs-2">
                    {item.referenceId || na}
                  </div>
                  <div className="col-xs-4">
                    {_.safe(item, "shopId.userId.firstName")} {_.safe(item, "shopId.userId.lastName", "")}
                  </div>
                  <div className="col-xs-4">
                    {moment(item.createdAt).format("MMM DD, YYYY hh:mm A")}
                  </div>
                  <div className="col-xs-2">
                    {item.status.toUpperCase()}
                  </div>
                </div>
            </a>
          </h4>
        </div>
        <div id={item.id+index}
          className={ClassNames("panel-collapse collapse")}
          role="tabpanel"
          aria-labelledby={"heading" + index}>
            <div className="panel-body">
                <p>
                  <strong>From Shop: </strong>
                  <Link to={"shop/" + _.safe(item, "shopId.userId.id")} className="text-decor-none">
                    <strong className="text-default">
                      {_.safe(item, "shopId.userId.firstName")} {_.safe(item, "shopId.userId.lastName")}
                    </strong>
                  </Link>
                </p>
                <p>
                  <strong>Order Total: </strong>
                  <strong className="text-success"> ₱ {total + handlingFee}</strong>
                </p>
                <p>
                  <strong>Billing Address: </strong>
                  {address.streetAddress},&nbsp;
                  {address.city}&nbsp;
                  {address.province}&nbsp;
                  {address.country}&nbsp;
                  {address.zipcode}
                </p>
                <p>
                  <strong>Paid thru: </strong>
                  {item.paymentPlatform.toUpperCase()}
                </p>
                <p className="hide">You will receive this in 1-2 business days</p>
                <p className="text-uppercase">This order contains the following item(s):</p>
                <table className="table table-bordered">
                  <thead>
                    <tr>
                      <th>Item Details</th>
                      <th className="text-right">Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    {item.items.map(this.listProducts)}
                    <tr>
                      <td className="text-right">Shipping:</td>
                      <td className="text-right">₱ {totalShipping}</td>
                    </tr>
                    <tr>
                      <td className="text-right">Handling Fee:</td>
                      <td className="text-right">₱ {handlingFee}</td>
                    </tr>
                    <tr>
                      <td className="text-right">Grand Total:</td>
                      <td className="text-right"><strong className="text-success">₱ {total + handlingFee}</strong></td>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
      </div>
    )
  }

  renderSetStatus () {
    return (
      <select className="btn btn-primary">
        <option>PENDING</option>
        <option>RECEIVED</option>
      </select>
    )
  }


  listProducts (item, index) {
    return (
      <tr key={index}>
        <td>
          {item.quantity} x &nbsp;
          <Link to={"/product/" + item.productId.id}>
            {item.productId.name}
          </Link>
        </td>
        <td className="text-right">₱ {item.quantity * item.productId.price}</td>
      </tr>
    )
  }
};

