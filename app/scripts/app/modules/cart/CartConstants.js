"use strict";

import keyMirror from "keymirror";

export default keyMirror({
  CART_CHANGE_EVENTS : null,
  CART_INITIALIZE : null,
  CART_RESET : null,
  CART_UPDATE_QUANTITY : null,
  CART_REMOVE_ITEM : null,
  CART_PROCEED : null
});
