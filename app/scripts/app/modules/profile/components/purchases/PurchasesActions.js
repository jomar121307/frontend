"use strict";

import AppDispatcher from "../../../../dispatcher/AppDispatcher.js";
import PurchasesConstants from "./PurchasesConstants.js";

const PurchasesActions = {
  initialize : function (options) {
    AppDispatcher.handleViewAction({
      type : PurchasesConstants.PURCHASES_INITIALIZE,
      params : {
        options
      }
    });
  }
};

export default PurchasesActions;
