import React from 'react';

export default class MessageOwner extends React.Component {
  constructor(props) {
    super(props);

    this.handleSend = this.handleSend.bind(this);
  }

  render() {
    const {user} = this.props;
    let thumb = _.safe(user, "metadata.profileVideo.secure_url", config.defaultYoutubeProfileVideo);

    if (_.safe(user, "metadata.profileVideo.resource_type", "youtube") === "youtube") {
      thumb = cloudinary.url(`https://i.ytimg.com/vi/${thumb}/default.jpg`, {
        height: 45,
        width: 45,
        crop: "fill",
        type : "fetch"
      });
    } else if (_.safe(user, "metadata.profileVideo.resource_type") === "video") {
      const profileVideo = user.metadata.profileVideo;

      thumb = cloudinary.url(`${profileVideo.public_id}.jpg`, {
        height: 45,
        transformation: ["media_lib_thumb"],
        width: 45,
        crop: "fill",
        resource_type: "video",
        secure : true
      });
    }

    return (
      <div className="modal fade" id="messageOwner" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 className="modal-title" id="myModalLabel">
                <span className="message-to">Message to</span> {user.firstName} {user.lastName || ""}
              </h4>
            </div>
            <div className="modal-body">
              <div className="media">
                <div className="media-left media-top">
                  <img className="media-object" src={thumb} alt={user.firstName + " " + (user.lastName || "")} />
                </div>
                <div className="media-body">
                  <textarea className="form-control contact-owner-textarea"
                    ref="message"
                    placeholder="Your message here.">
                  </textarea>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">
                Close
              </button>
              <button type="button" className="btn btn-primary" onClick={this.handleSend}>
                Send Message
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleSend (e) {
    const {handleSend} = this.props;

    if (this.refs["message"].value.trim()) {
      handleSend(this.refs["message"].value.trim());
      this.refs["message"].value = "";

      $("#messageOwner").modal("hide");
    }
  }
}

MessageOwner.propTypes = {
  handleSend : React.PropTypes.func.isRequired
}
